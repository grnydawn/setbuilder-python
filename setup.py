# -*- coding: utf-8 -*-

from setuptools import setup, find_packages

setup(
    name='sbpy',
    version='0.1.0',
    description='SetBuilder-Python package',
    long_description='T.B.D.',
    author='Youngsung Kim',
    author_email='grnydawn@gmail.com',
#    url='T.B.D.',
    license='T.B.D.',
    packages=find_packages(exclude=('tests', 'docs'))
)
