.. sbpy documentation master file, created by
   sphinx-quickstart on Thu Jan 12 20:42:06 2017.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to sbpy's documentation!
================================

Have you ever wondered why you can not specify a set in any programming language like?:

{ x | x > 0 where x in REAL_NUMBER }

This Python library tries to provde a novel answer to this wondering.

Contents:

.. toctree::
   :maxdepth: 2


Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`

