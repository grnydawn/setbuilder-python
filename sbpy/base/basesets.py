# -*- coding: UTF-8 -*-

"""Set classes
"""

import sys
import inspect

#############################################
# Common
#############################################

class Set(object):
    """Parent class of all sbpy Set classes.

    **Set** class defines public interface of sbpy Sets.
    """

    def __init__(self, setbld, frozen=True, ordered=False, gids=None, lids=None):

        # preprocess
        if gids:
            self.gids = dict(gids)
        else:
            self.gids = dict(inspect.currentframe().f_back.f_globals)

        if lids:
            self.lids = dict(lids)
        else:
            self.lids = dict(inspect.currentframe().f_back.f_locals)

        # set creation
        self._init_(setbld)

        # postprocess
        self.frozen = frozen
        self.ordered = ordered

    def _init_(self):
        raise Exception('"_init_" should be implemented.')

    def __str__(self):
        raise Exception('"__str__" should be implemented.')

    def __contains__(self, elem):
        raise Exception('"__contains__" should be implemented.')

    def choice(self, max_cases, case_reuse=False):
        raise Exception('"choice" should be implemented.')

#############################################
# PrimitiveSet
#############################################

class PrimitiveSet(Set):
    def _init_(self):
        pass

    def __str__(self):
        return self.__class__.__name__

    def choice(self, max_cases, case_reuse=False):
        pass

#############################################
# EnumSet
#############################################

class EnumSet(Set):
    def _init_(self, elems):
        self.elems = []

        if isinstance(elems, EnumSet):
            self.elems.extend(list(elems.elems))
        else:
            for elem in elems:
                self.elems.append(elem)

    def __str__(self):
        l = []
        for e in self.elems:
            if isinstance(e, Set):
                l.append(e._sb___str__(gids,lids, **opts))
            elif isinstance(e, str):
                l.append('"%s"'%e)
            else:
                l.append(str(e))
        return '{ %s }'%', '.join(l)

    def choice(self, max_cases, case_reuse=False):
        pass

#############################################
# PredicateSet
#############################################

class PredicateBase(object):

    def __init__(self, *args, **kwargs):

        # preprocess
        gids = kwargs.get('gids', None)
        if gids:
            self.gids = dict(gids)
        else:
            self.gids = dict(inspect.currentframe().f_back.f_globals)

        lids = kwargs.get('lids', None)
        if lids:
            self.lids = dict(lids)
        else:
            self.lids = dict(inspect.currentframe().f_back.f_locals)

        # predicate object creation
        self._init_(*args)

    def _init_(self, *args):
        raise Exception('"_init_" should be implemented by a sub-class.')

    def __str__(self):
        raise Exception('"__str__" should be implemented by a sub-class.')

class SetVar(PredicateBase):
    def _init_(self, vartuple):
        self.vartuple = tuple(vartuple)

    def __str__(self):
        if len(self.vartuple) > 1:
            return '( %s )'%', '.join([ str(v) for v in self.vartuple])
        else:
            return str(self.vartuple[0])

    def comply(self, elem):
        if isinstance(elem, ( list, tuple, set ) ):
            return len(self.vartuple) == len(elem)
        elif len(self.vartuple) == 1:
            return True
        return False

class RuleSetVar(SetVar):
    varmapname = 'x'

class WhereSetVar(SetVar):
    varmapname = 'y'

class WherePart(PredicateBase):
    def _init_(self, wherebodylist):
        self.wherebodylist = wherebodylist

    def __str__(self):
        return 'where %s'%' and '.join([ str(wherebody) for wherebody in self.wherebodylist])

class WhereBody(PredicateBase):
    def _init_(self, setvar, setexpr):
        self.setvar = setvar
        self.setexpr = setexpr

        if isinstance(setexpr, Set):
            self.setobj = setexpr
        else:
            raise Exception('Not supported type: %s'%type(setexpr))

    def __str__(self):
        return "%s in %s"%(str(self.setvar), str(self.setexpr))

class WhereBracketBody(PredicateBase):
    def _init_(self, setvar, setexpr, repeat):
        self.setvar = setvar
        self.setexpr = setexpr
        self.repeat = repeat

        if isinstance(setexpr, Set):
            self.setobj = setexpr
        else:
            raise Exception('Not supported type: %s'%type(setexpr))

    def __str__(self):
        return "%s in [ %s ] * %s"%(str(self.setvar), str(self.setexpr), str(self.repeat))

class SetTester(PredicateBase):
    def _init_(self, test, setvar):
        self.test = test
        self.setvar = setvar

    def __str__(self):
        return str(self.test)

    def comply(self, elem, varname):
        '''Check if elem satisfies set test

            NOTE: The last statment of set rule should only use set variable name in SetVar
        '''

        try:
            for _ in elem: break
        except:
            elem = ( elem, )

        if len(elem) != len(varname):
            return False

        lids_copy = dict(self.lids)

        sbmod = sys.modules['sbpy']
        for k in dir(sbmod):
            if k.startswith('SB_'):
                lids_copy[k] = getattr(sbmod, k, None)

        for v, e in zip(varname, elem):
            lids_copy[v] = e

        result = False
        try:
            ##print ('BBB', str(self.test), str(lids_copy))
            exec('_sb_result = %s'%str(self.test), self.gids, lids_copy)
            return lids_copy['_sb_result']
        except:
            pass

        return result

class SetGenerator(PredicateBase):
    def _init_(self, func, setrulevar, setwherevarlist):
        self.func = func
        self.setrulevar = setrulevar
        self.setwherevarlist = setwherevarlist

    def __str__(self):
        return str(self.func)

class SetRule(PredicateBase):
    def _init_(self, setvar, settester, setgenerator, wherepart):
        self.setvar = setvar
        self.settester = settester
        self.setgenerator = setgenerator
        self.wherepart = wherepart

    def __str__(self):
        if self.setgenerator and self.wherepart:
            return "%s | %s | %s %s"%(str(self.setvar), str(self.settester), \
                str(self.setgenerator), str(self.wherepart))
        elif self.wherepart:
            return "%s | %s %s"%(str(self.setvar), str(self.settester), \
                str(self.wherepart))
        else:
            return "%s | %s"%(str(self.setvar), str(self.settester))

    def contains(self, elem):
        return self.setvar.comply(elem) and self.settester.comply(elem, self.setvar.vartuple)

class PredicateSet(Set):

    def _init_(self, setrule):
        self.setrule = setrule

    def __str__(self):
        return '{ %s }'%str(self.setrule)

    def __contains__(self, elem):
        return self.setrule.contains(elem)

    def choice(self, max_cases, case_reuse=False):
        return self.setrule.choice(max_cases, case_reuse)

###########################################################################
# Compound  set
###########################################################################

class CompoundSet(Set):
    def _init_(self, setbld):

        self.left = setbld[0]
        self.op = setbld[1]
        self.right = setbld[2]
        self.paren = False

    def __str__(self):
        if self.paren:
            return '( %s %s %s )'%(str(self.left), str(self.op), str(self.right))
        else:
            return '%s %s %s'%(str(self.left), str(self.op), str(self.right))

    def choice(self, max_cases, case_reuse=False):
        pass


#############################################
# MixedSet
#############################################

class MixedSet(Set):
    def _init_(self, elems):
        pass

    def __str__(self):
        pass

    def choice(self, max_cases, case_reuse=False):
        pass

#############################################
# Functions
#############################################


def set_from_ID(setid, gids, lids):

    if lids and setid in lids:
        if inspect.isclass(lids[setid]) and issubclass(lids[setid], Set):
            return lids[setid](None, gids=gids, lids=lids)
        else:
            return lids[setid]

    if gids and setid in gids:
        if inspect.isclass(gids[setid]) and issubclass(gids[setid], Set):
            return gids[setid](None, gids=gids, lids=lids)
        else:
            return gids[setid]

    # if not find from builtin set
    if isinstance(setid, str):
        #if name == 'SB_INDEX_SET':
        #    return SB_INDEX_SET()

        import builtinsets as bis
        if setid in dir(bis):
            setid = getattr(bis, setid, setid)
        if inspect.isclass(setid) and issubclass(setid, PrimitiveSet):
            setid = setid(None, gids=gids, lids=lids)
    return setid

