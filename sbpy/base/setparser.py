# -*- coding: UTF-8 -*-
"""Parse a setnote and generate a sbpy set object

"""

import re
import basesets

SB_PARSED_INSTANCES = []
_SBSTR_ = r'__SB_STRMAP_'
strmap = {}
DEBUG = True

def prep_quote(s):
    """convert quotes into strid and generate string map
    """

    strmapid = 0

    if not s: return s

    escape = False
    quotechar = None
    retstr = []
    quotestr = None
    for _s in s:
        if quotestr is None:
            if escape or _s == "\\":
                raise Exception('Wrong escaping')
            elif _s in [ '"', "'" ]:
                quotechar = _s
                quotestr = []
            else:
                retstr.append(_s)
        else:
            if escape:
                quotestr.append(_s)
                escape = False
            elif _s == "\\":
                quotestr.append(_s)
                escape = True
            elif _s in [ '"', "'" ]:
                if quotechar == _s:
                    key = '__SB_STRMAP_%d'%strmapid
                    strmapid += 1
                    strmap[key] = quotechar + ''.join(quotestr) + quotechar
                    retstr.append(key)
                    quotestr = None
                else:
                    quotestr.append(_s)
            else:
                quotestr.append(_s)

    return ''.join(retstr)

def apply_map(match):
    matchstr = match.group()
    if matchstr in strmap:
        return '%s'%strmap[matchstr]
    else:
        return matchstr

def apply_strmap(match):
    matchstr = match.group()
    if matchstr in strmap:
        return '%s'%strmap[matchstr][1:-1]
    else:
        return matchstr

def preprocess(s):
    #if not isinstance(s, unicode):
    #    s = unicode(s, 'utf-8')
    s = prep_quote(s)
    s = s.replace('where', '#')
    return s


#############################################
# logging
#############################################

import logging
logging.basicConfig(
    level = logging.DEBUG,
    filename = u"parselog.txt",
    filemode = u"w",
    format = u"%(filename)10s:%(lineno)4d:%(message)s"
)
log = logging.getLogger()


#############################################
# LEXER
#############################################

#############################################
# states
#############################################

states = (
   (u'set',u'exclusive'),
   (u'py',u'exclusive'),
   (u'where',u'exclusive'),
)

#############################################
# tokens
#############################################

tokens = (
    'STMTS', 'WHERE', 'VBAR', 'IN', 'COMMA', 'AMPHERSAND', 'INT', 'FLOAT',
    'SCI', 'RBRACE', 'LBRACE', 'RPAREN', 'LPAREN', 'MINUS', 'AND', 'ID',
    'LBRACKET', 'RBRACKET', 'ASTERISK', 'CARET', 'STR'
    )

#############################################
# ANY state
#############################################

t_ANY_LPAREN    = r'\('
t_ANY_RPAREN    = r'\)'
t_ANY_COMMA     = r','
t_ANY_AMPHERSAND= r'&'
t_ANY_MINUS     = r'-'
t_ANY_CARET     = r'\^'
t_ANY_INT       = r'[+-]?[0-9]+'
t_ANY_FLOAT     = r'[+-]?([0-9]*\.[0-9]+|[0-9]+\.[0-9]*)'
t_ANY_SCI       = r'[+-]?([0-9]*\.[0-9]+|[0-9]+\.[0-9]*)[eE][+-]?[0-9]+'
t_ANY_STR       = _SBSTR_ + r'\d+'

# Ignored characters
t_ANY_ignore = " \t"

def t_ANY_LBRACE(t):
    r'\{'
    t.lexer.push_state('set')
    return t

def t_ANY_RBRACE(t):
    r'\}'
    t.lexer.pop_state()
    return t

def t_ANY_newline(t):
    r'\n+'
    t.lexer.lineno += t.value.count("\n")

def t_ANY_error(t):
    print("Illegal character '%s'" % t.value[0])
    t.lexer.skip(1)

#############################################
# INITIAL state
#############################################


t_ID            = r'[^\d\W]\w*'
t_VBAR          = r'\|'

#############################################
# set state
#############################################

t_set_ID        = r'[^\d\W]\w*'

def t_set_VBAR(t):
    r'\|'
    t.lexer.begin('py')
    return t

#############################################
# py state
#############################################

t_py_STMTS      = r'[^\{\}\|\#]+'
t_py_VBAR       = r'\|'

def t_py_WHERE(t):
    r'\#'
    t.lexer.begin('where')
    return t

#############################################
# where state
#############################################

t_where_LBRACKET= r'\['
t_where_RBRACKET= r'\]'
t_where_ASTERISK= r'\*'
t_where_VBAR    = r'\|'
t_where_IN      = r'in'
t_where_AND     = r'and'
t_where_ID      = r'(?!(in|and))[^\d\W]\w*'

# Build the lexer
import ply.lex as lex
lexer = lex.lex(reflags=re.UNICODE, debug=1, debuglog=log, errorlog=log)

#############################################
# PARSER
#############################################

def p_error(t):
    if t:
        print("Syntax error at '%s' (type=%s, lineno=%d, lexpos=%d)" % (t.value, t.type, t.lineno, t.lexpos))
    else:
        print("Syntax error")
    import sys; sys.exit(-1)

precedence = (
    ('left','MINUS', 'COMMA'),
    ('left','VBAR', 'AMPHERSAND', 'AND'),
    ('right', 'WHERE', 'IN'),
    )

def p_compset(t):
    '''compset :  ID
        | enumset
        | predicateset
        | compset set_op enumset
        | compset set_op predicateset
        | compset set_op ID
        | listcompset
    '''
    if DEBUG: print(t.slice)

    if len(t) == 2:
        if t.slice[1].type == 'ID':
            t[0] = basesets.set_from_ID(t[1], t.parser.gids, t.parser.lids)
        else:
            t[0] = t[1]
    elif len(t) == 4:
        if t.slice[3].type == 'ID':
            t[0] = basesets.CompoundSet((t[1], t[2], basesets.set_from_ID(t[3], t.parser.gids, t.parser.lids)), \
                gids=t.parser.gids, lids=t.parser.lids)
        else:
            t[0] = basesets.CompoundSet((t[1], t[2], t[3]), gids=t.parser.gids, lids=t.parser.lids)
        SB_PARSED_INSTANCES.append(t[0])

def p_listcompset(t):
    '''listcompset : LPAREN compset RPAREN
        | listcompset set_op compset
    '''
    if DEBUG: print(t.slice)

    if len(t) == 4:
        if t[1] == '(' and t[3] == ')':
            t[0] = t[2]
            if isinstance(t[0], basesets.CompoundSet):
                t[0].paren = True
        elif t[2] in [ '|', '&', '-' ]:
            t[0] = basesets.CompoundSet((t[1], t[2], t[3]), gids=t.parser.gids, lids=t.parser.lids)
            SB_PARSED_INSTANCES.append(t[0])
        else:
            raise Exception('Syntax error at: %s, %s, %s'%(str(t[1]), str(t[2]), str(t[3])))

def p_set_op(t):
    '''set_op : VBAR
        | MINUS
        | AMPHERSAND
        | CARET
    '''
    if DEBUG: print(t.slice)

    if len(t) == 2:
        t[0] = t[1]

def p_enumset(t):
    '''enumset : LBRACE RBRACE
        | LBRACE elems RBRACE
    '''
    if DEBUG: print(t.slice)

    if len(t) == 3:
        t[0] = basesets.EnumSet([], frozen=t.parser.frozen, ordered=t.parser.ordered,
            gids=t.parser.gids, lids=t.parser.lids)
        SB_PARSED_INSTANCES.append(t[0])
    elif len(t) == 4:
        if any( isinstance(elem, basesets.SetRule) for elem in t[2] ):
            t[0] = basesets.MixedSet(t[2], frozen=t.parser.frozen, ordered=t.parser.ordered,
                gids=t.parser.gids, lids=t.parser.lids)
        else:
            t[0] = basesets.EnumSet(t[2], frozen=t.parser.frozen, ordered=t.parser.ordered,
                gids=t.parser.gids, lids=t.parser.lids)
        SB_PARSED_INSTANCES.append(t[0])

def p_predicateset(t):
    '''predicateset : LBRACE set_rule RBRACE
    '''
    if DEBUG: print(t.slice)

    if len(t) == 4:
        t[0] = basesets.PredicateSet(t[2], gids=t.parser.gids, lids=t.parser.lids)
        SB_PARSED_INSTANCES.append(t[0])

def p_elems(t):
    '''elems : elem
        | elems COMMA elem
    '''
    if DEBUG: print(t.slice)

    if len(t) == 2:
        t[0] = [t[1]]
    elif len(t) == 4:
        if t[1] is None:
            t[1] = []
        t[0] = t[1] + [t[3]]

def p_elem(t):
    '''elem : ID
        | STR
        | number
        | enumset
        | setvar
        | set_rule
    '''
    if DEBUG: print(t.slice)

    if len(t) == 2:
        if t.slice[1].type == 'STR':
            t[0] = re.sub(r'%s\d+'%_SBSTR_, apply_strmap, t[1])
        elif t.slice[1].type == 'ID':
            t[0] = basesets.set_from_ID(t[1], t.parser.gids, t.parser.lids)
        else:
            t[0] = t[1]

def p_set_rule(t):
    """set_rule : setvar VBAR STMTS VBAR STMTS wherepart
        | setvar VBAR STMTS wherepart
        | setvar VBAR STMTS
    """
    if DEBUG: print(t.slice)

    setvar = basesets.RuleSetVar(t[1], gids=t.parser.gids, lids=t.parser.lids)
    tester = basesets.SetTester(re.sub(r'%s\d+'%_SBSTR_, apply_map, t[3]), setvar, gids=t.parser.gids, lids=t.parser.lids)

    if len(t) == 7:
        generator = basesets.SetGenerator(re.sub(r'%s\d+'%_SBSTR_, apply_map, t[5]), \
            setvar, [wbl.setvar for wbl in t[6].wherebodylist], gids=t.parser.gids, lids=t.parser.lids)
        t[0] = basesets.SetRule(setvar, tester, generator, t[6], gids=t.parser.gids, lids=t.parser.lids)
    elif len(t) == 5:
        t[0] = basesets.SetRule(setvar, tester, None, t[4], gids=t.parser.gids, lids=t.parser.lids)
    elif len(t) == 4:
        t[0] = basesets.SetRule(setvar, tester, None, None, gids=t.parser.gids, lids=t.parser.lids)


def p_setvar(t):
    '''setvar : ID
        | LPAREN subvars RPAREN
    '''
    if DEBUG: print(t.slice)

    if len(t) == 2:
        t[0] = [t[1]]
    elif len(t) == 4:
        t[0] = t[2]

def p_subvars(t):
    '''subvars : ID
        | subvars COMMA ID
    '''
    if DEBUG: print(t.slice)

    if len(t) == 2:
        t[0] = [t[1]]
    elif len(t) == 4:
        if t[1] is None:
            t[1] = []
        t[0] = t[1] + [t[3]]

def p_wherepart(t):
    'wherepart : WHERE wherebodylist'
    if DEBUG: print(t.slice)

    if len(t) == 3:
        t[0] = basesets.WherePart(t[2], gids=t.parser.gids, lids=t.parser.lids)

def p_wherebodylist(t):
    """wherebodylist : wherebody
        | wherebodylist AND wherebody
    """
    if DEBUG: print(t.slice)

    if len(t) == 2:
        t[0] = [t[1]]
    elif len(t) == 4:
        if t[1] is None:
            t[1] = []
        t[0] = t[1] + [t[3]]

def p_wherebody(t):
    '''wherebody : setvar IN compset
        | setvar IN LBRACKET compset RBRACKET ASTERISK integer
    '''
    if DEBUG: print(t.slice)

    if len(t) == 4:
        if t.slice[3].type == 'ID':
            t[0] = basesets.WhereBody(basesets.WhereSetVar(t[1]), basesets.set_from_ID(t[3], t.parser.gids, t.parser.lids), gids=t.parser.gids, lids=t.parser.lids)
        else:
            t[0] = basesets.WhereBody(basesets.WhereSetVar(t[1]), t[3], gids=t.parser.gids, lids=t.parser.lids)
    elif len(t) == 8:
        if t.slice[4].type == 'ID':
            t[0] = basesets.WhereBracketBody(basesets.WhereSetVar(t[1]), basesets.set_from_ID(t[4], t.parser.gids, t.parser.lids), t[7], gids=t.parser.gids, lids=t.parser.lids)
        else:
            t[0] = basesets.WhereBracketBody(basesets.WhereSetVar(t[1]), t[4], t[7], gids=t.parser.gids, lids=t.parser.lids)

def p_number(t):
    '''number : integer
        | float
        | scientific
    '''
    if DEBUG: print(t.slice)

    t[0] = t[1]

def p_integer(t):
    'integer : INT'
    if DEBUG: print(t.slice)

    t[0] = int(t[1])

def p_float(t):
    'float : FLOAT'
    if DEBUG: print(t.slice)

    t[0] = float(t[1])

def p_scientific(t):
    'scientific : SCI'
    if DEBUG: print(t.slice)

    t[0] = float(t[1])

import ply.yacc as yacc

def parse(setnote, frozen, ordered, setglobals, setlocals):

    # removing previous sets
    del SB_PARSED_INSTANCES[:]

    # preprocessing
    prep_setnote = preprocess(setnote)

    # parsing
    parser = yacc.yacc(debug=True, debuglog=log, errorlog=log)
    parser.gids = setglobals
    parser.lids = setlocals
    parser.frozen = frozen
    parser.ordered = ordered

    parser.parse(prep_setnote, debug=log)

    # return a set or None
    if len(SB_PARSED_INSTANCES) == 0:
        return None
    else:
        if isinstance(SB_PARSED_INSTANCES[-1], basesets.Set):
            return SB_PARSED_INSTANCES[-1]
        else:
            raise Exception('Last class is not Set class.')

if __name__ == "__main__":
    import inspect

    setglobals = inspect.currentframe().f_globals
    setlocals = inspect.currentframe().f_locals

    while True:
        try:
            s = raw_input('setbuilder > ')   # Use raw_input on Python 2
        except NameError:
            s  = input('setbuilder > ')   # Use input on Python 3
        except EOFError:
            break

        sbset = parse(s, True, False, setglobals, setlocals)
        if sbset:
            print (sbset)
        else:
            print ('Failed')

