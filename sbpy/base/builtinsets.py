# -*- coding: UTF-8 -*-
"""sbpy built-in sets
"""

import inspect
import setparser
import basesets

#############################################
# Functions
#############################################

def primset_build(setnote):
    frame = inspect.currentframe().f_back
    return setparser.parse(setnote, True, True, frame.f_globals, frame.f_locals)

###########################################################################
# Primitive Sets
###########################################################################

# Test Functions
SB_BOOLEAN_TEST        = lambda x: type(x) == type(True)
SB_INT_TEST            = lambda x: type(x) == type(1)
SB_FLOAT_TEST          = lambda x: type(x) == type(1.0)

class SB_BOOLEAN_SET(basesets.PrimitiveSet):

    def _init_(self, dummy):
        pass

class SB_INTNUM_SET(basesets.PrimitiveSet):

    def _init_(self, dummy):
        pass

class SB_FLOATNUM_SET(basesets.PrimitiveSet):

    def _init_(self, dummy):
        pass

class SB_CHAR_SET(basesets.PrimitiveSet):

    def _init_(self, dummy):
        pass

class SB_UNICODE_SET(basesets.PrimitiveSet):

    def _init_(self, dummy):
        pass

###########################################################################
# Number Sets
###########################################################################


# Test Functions
SB_NATURAL_TEST         = lambda x: SB_INT_TEST(x) and x > 0
SB_EVENNUM_TEST         = lambda x: SB_NATURAL_TEST(x) and x%2 == 0
SB_ODDNUM_TEST          = lambda x: SB_NATURAL_TEST(x) and x%2 == 1
SB_REALNUM_TEST         = lambda x: SB_INT_TEST(x) or SB_FLOAT_TEST(x)
SB_COMPLEXNUM_TEST      = lambda x: type(x) == type(1j)
SB_NUM_TEST             = lambda x: SB_REALNUM_TEST(x) or SB_COMPLEXNUM_TEST(x)


# Set construction
SB_NATURALNUM_SET       = primset_build('{ x | SB_NATURAL_TEST(x) | x = n where n in SB_INTNUM_SET }')
SB_ODDNUM_SET           = primset_build('{ x | SB_ODDNUM_TEST(x) | x = n where n in SB_INTNUM_SET }')
SB_EVENUM_SET           = primset_build('{ x | SB_EVENNUM_TEST(x)| x = n where n in SB_INTNUM_SET }')
SB_REALNUM_SET          = primset_build('{ x | SB_REALNUM_TEST(x) | x = n where n in SB_INTNUM_SET | SB_FLOATNUM_SET }')
SB_COMPLEXNUM_SET       = primset_build('{ x | SB_COMPLEXNUM_TEST(x) | x = complex(n[0], n[1]) \
                            where n in [ SB_FLOATNUM_SET | SB_FLOATNUM_SET ] * 2 }')
SB_NUM_SET              = primset_build('{ x | SB_NUM_TEST(x) | x = n where n in SB_REALNUM_SET | SB_COMPLEXNUM_SET }')

###########################################################################
# Character Sets
###########################################################################

# Test Functions
SB_ASCII_TEST           = lambda x: x.encode('ascii')
SB_LATIN1_TEST          = lambda x: x.encode('latin1')
SB_ASCIIPRINTABLE_TEST  = lambda x: SB_ASCII_TEST(x) and ord(x) >= 32
SB_ASCIICONTROL_TEST    = lambda x: SB_ASCII_TEST(x) and ord(x) < 32
SB_ASCIIEXTENDED_TEST   = lambda x: SB_LATIN1_TEST() and ord(x) >= 127

# Set construction
SB_ASCII_SET            = primset_build('{ x | SB_ASCII_TEST(x) | x = c where c in SB_CHAR_SET }')
SB_LATIN1_SET           = primset_build('{ x | SB_LATIN1_TEST(x) | x = c where c in SB_CHAR_SET }')
SB_ASCIIPRINTABLE_SET   = primset_build('{ x | SB_ASCIIPRINTABLE_TEST(x) | x = c where c in SB_CHAR_SET }')
SB_ASCIIEXTENDED_SET    = primset_build('{ x | SB_ASCIIEXTENDED_TEST(x) | x = c where c in SB_CHAR_SET }')
SB_ASCIICONTROL_SET     = primset_build('{ x | SB_ASCIICONTROL_TEST(x) | x = c where c in SB_CHAR_SET }')

