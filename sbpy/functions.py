# -*- coding: utf-8 -*-
"""This module contains sbpy public functions"""

import inspect
from .base import setparser

def build(setnote, frozen=True, ordered=False, setglobals=None, setlocals=None):
    """This function builds a sbpy set object from a string of sbpy set specification.

    :param setnote: a set description
    :type setnote: str
    :returns: a Set object
    :rtype: Set

    :Example:

    >>> import sbpy
    >>> sbpy.build('{}')
    sbpy.Set()

    .. note:: 개발을 위한 기본 세팅을 마치면 다음단계로 어떻게 설계할 것인가에 대한
         대답을 해 줄 툴이 없다. 어떤 설계상의 선택을 하면 어떤 결과를 얻게 될지
        모르기 때문에 시행착오를 오랜시간에 걸쳐 해야 한다. 어떻게 그것을 없애거나
        아니면 짧은 시간에 여러번 시행착오를 의도적으로 해서 설계를 발전시킬 수 는 없을까?
    """

    if not setglobals:
        setglobals = inspect.currentframe().f_back.f_globals

    if not setlocals:
        setlocals = inspect.currentframe().f_back.f_locals

    return setparser.parse(setnote, frozen, ordered, setglobals, setlocals)
