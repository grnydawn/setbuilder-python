# -*- coding: UTF-8 -*-

"""setbuilder-python parser tests

"""

import setbuilder as sb

def test_set_blank():
    setstr = "{}"
    s = sb.build(setstr)
    assert isinstance(s, sb.Set)

def test_set_enum():
    setstr = "{a,b,c}"
    s = sb.build(setstr)
    assert isinstance(s, sb.Set)

def test_set_num():
    setstr = "{1,2,3}"
    s = sb.build(setstr)
    assert isinstance(s, sb.Set)

def test_set_scinum():
    setstr = "{-1.1E-4,2.1e2,3.23E-03}"
    s = sb.build(setstr)
    orgvals = [ float(v) for v in setstr[1:-1].split(',') ]
    retvals = [ float(v) for v in str(s)[1:-1].split(',') ]
    assert isinstance(s, sb.Set)

def test_tester():
    setstr = "{x| x > 1}"
    s = sb.build(setstr)
    assert isinstance(s, sb.Set)

def test_set_union():
    setstr = "{a,b,c} | {b,c,d}"
    s = sb.build(setstr)
    assert isinstance(s, sb.Set)

def test_set_intersection():
    setstr = "{a,b,c} & {b,c,d}"
    s = sb.build(setstr)
    assert isinstance(s, sb.Set)

def test_set_difference():
    setstr = "{a,b,c} - {b,c,d}"
    s = sb.build(setstr)
    assert isinstance(s, sb.Set)

def test_set_symmdifference():
    setstr = "{a,b,c} ^ {b,c,d}"
    s = sb.build(setstr)
    assert isinstance(s, sb.Set)

def test_multiset():
    setstr = "{z| z > 1 | z = x + y where x in SB_NUM_SET and y in SB_ASCII_SET}"
    s = sb.build(setstr)
    assert isinstance(s, sb.Set)

def test_setinset():
    setstr = "{z| z > 1 | z = x + k where x in SB_NUM_SET and k in { y | True | y = z where z in SB_NUM_SET}}"
    s = sb.build(setstr)
    assert isinstance(s, sb.Set)

def test_setop():
    setstr = "{y| y > 1 | y = x + k where x in SB_NUM_SET | SB_ASCII_SET and k in { z | True | z = a where a in SB_NUM_SET & SB_NUM_SET}}"
    s = sb.build(setstr)
    assert isinstance(s, sb.Set)

def test_compsetop():
    setstr = "SB_NUM_SET | { y | True | y where y in SB_NUM_SET & SB_NUM_SET}"
    s = sb.build(setstr)
    assert isinstance(s, sb.Set)

def test_listcompset():
    setstr = "SB_NUM_SET | { y | True | y where y in (SB_NUM_SET & SB_NUM_SET)}"
    s = sb.build(setstr)
    assert isinstance(s, sb.Set)

def test_listcompset2():
    setstr = "SB_NUM_SET | { y | True | y where y in (SB_NUM_SET & SB_NUM_SET) | ((SB_NUM_SET | SB_NUM_SET) & SB_NUM_SET)}"
    s = sb.build(setstr)
    assert isinstance(s, sb.Set)

def test_multivar():
    setstr = "{ (x,y) | True | x = z + 1; y = q + 2 where z in SB_NUM_SET and q in SB_NUM_SET}"
    s = sb.build(setstr)
    assert isinstance(s, sb.Set)

def test_multivar2():
    setstr = "{ (x,y) | True | x = z + 1; y = q + 2 where (z,q) in SB_NUM_SET}"
    s = sb.build(setstr)
    assert isinstance(s, sb.Set)

def test_bracket():
    setstr = "{ y | True | y = sum(x) where x in [ SB_NUM_SET ] * 10 }"
    s = sb.build(setstr)
    assert isinstance(s, sb.Set)

def test_foriegnchar():
    setstr = "{ y | True | y = u where u in { '하나', '둘', '셋' } }"
    s = sb.build(setstr)
    assert isinstance(s, sb.Set)

