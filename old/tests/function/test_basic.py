# -*- coding: UTF-8 -*-

"""setbuilder-python basic tests

"""

import setbuilder as sb

def test_parse():
    setstr = '{}'
    s = sb.build(setstr)
    assert str(s).replace(u' ', u'') == setstr.replace(u' ', u'')

def test_membership():
    setstr = "{ x | x > 1}"
    s = sb.build(setstr)
    assert s and 2 in s

def test_choice():
    setstr = "{ 1, 2, 3 }"
    s = sb.build(setstr)
    g = s.choice(2)
    assert s and len(g) == 2 and all( z in [ 1, 2, 3] for z in g )

def test_sbid():
    setstr = "{ a | a > 10 | a = n where n in SB_NUM_SET }"
    s = sb.build(setstr)

    assert len(s.sbid()) == 64 and type(int(s.sbid(), 16)) == type(1L)

def test_export():
    LARGERTHANONE = lambda x: x > 1
    setstr = "{ a | LARGERTHANONE(a) | a = n where n in SB_NATURALNUM_SET }"
    s = sb.build(setstr)
    g = s.choice(2)
    assert s and len(g) == 2 and all( z > 1 for z in g )
    assert sb.export(g, 'temp/export_g.py')
    assert sb.export(s, 'temp/export_s.py')
