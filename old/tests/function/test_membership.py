# -*- coding: UTF-8 -*-

"""setbuilder-python basic tests

"""

import setbuilder as sb

def test_neg_membership():
    setstr = "{ x | x > 1 }"
    s = sb.build(setstr)
    assert s and 1 not in s

def test_multistmts():
    setstr = "{ x | x**2 + x + 1 > 1 }"
    s = sb.build(setstr)
    assert s and 1 in s

def test_composite():
    setstr = "{ x | (x**2 + x + 1 > 1) and ( x < 10 ) }"
    s = sb.build(setstr)
    assert s and 10 not in s

def test_locals():
    one = lambda : 1
    setstr = "{ x | x > 1 + one()}"
    s = sb.build(setstr)
    assert s and 3 in s

def test_setops():

    setstr1 = "{ 1, 2, 3 }"
    s1 = sb.build(setstr1, frozen=False)
    setstr2 = "{ 2, 3, 4 }"
    s2 = sb.build(setstr2)

    s3 = s1 | s2
    assert s1 and s2 and s3 and sorted(tuple(s3)) == sorted(( 1, 2, 3, 4 ))

    s3 = s1 & s2
    assert s1 and s2 and s3 and sorted(tuple(s3)) == sorted(( 2, 3 ))

    s3 = s1 - s2
    assert s1 and s2 and s3 and sorted(tuple(s3)) == sorted(( 1, ))

    s3 = s1 ^ s2
    assert s1 and s2 and s3 and sorted(tuple(s3)) == sorted(( 1, 4 ))

    s1 |= s2
    assert s1 and s2 and sorted(tuple(s1)) == sorted(( 1, 2, 3, 4 ))

    s1 &= s2
    assert s1 and s2 and sorted(tuple(s1)) == sorted(( 2, 3, 4 ))

    s1 -= s2
    assert s2 and len(s1) == 0

    s1 ^= s2
    assert s1 and s2 and sorted(tuple(s1)) == sorted(( 2, 3, 4 ))

def test_builtinsets():
    senum = sb.build('{"abc", "def", "geh"}')

    sbool = sb.SB_BOOLEAN_SET()
    assert sbool and True in sbool

    snum = sb.SB_NUM_SET
    assert snum and all( num in snum for num in [0, 1.0, 1+1j] )

    schar = sb.SB_ASCIIPRINTABLE_SET
    assert schar and all( ch in schar for ch in ['a', '1', '!'] )

    sc =  senum | sbool | snum | schar
    assert sc and all( val in sc for val in ['abc', True, False, 0, 1.0, 1+1j, 'a', '1', '!'] )

    sc =  schar | senum | sbool | snum
    assert sc and all( val in sc for val in ['abc', True, False, 0, 1.0, 1+1j, 'a', '1', '!'] )

    sc =  sbool | snum | schar | senum
    assert sc and all( val in sc for val in ['abc', True, False, 0, 1.0, 1+1j, 'a', '1', '!'] )

def test_mixed():
    setstr = "{ y | True | y = n where n in { 1, 2, 3, 4 } ^ { 3, 4, 5, 6 } }"
    s = sb.build(setstr)
    g = s.choice(4)
    r = g & sb.build('{ k | k > 3 | k = q where q in SB_INTNUM_SET }')
    x = r.choice(2)
    y = sb.build('{ 5, 6 }')

    assert x and y and x == y

