# -*- coding: UTF-8 -*-

"""setbuilder-python basic tests

"""

import setbuilder as sb

class MyClass(object):

    def __init__(self, myvar):
        self.myvar = myvar

def test_rule():
    setstr = "{ z | z > 100 | z = x + 1 where x in SB_INTNUM_SET }"
    s = sb.build(setstr)
    g = s.choice(10)
    assert s and len(g) == 10 and all( isinstance(z, int) and z > 100 for z in g )

def test_compound():
    setstr = "{ z | z > 100 | z = x + 1 where x in SB_REALNUM_SET }"
    s = sb.build(setstr)
    g = s.choice(10)
    assert s and len(g) == 10 and all( (isinstance(z, int) or isinstance(z, float)) and z > 100 for z in g )

def test_twosets():
    setstr = "{ z | z > 100 | z = max(x, y) where x in SB_REALNUM_SET and y in SB_REALNUM_SET }"
    s = sb.build(setstr)
    g = s.choice(10)
    assert s and len(g) == 10 and all( (isinstance(z, int) or isinstance(z, float)) and z > 100 for z in g )

def test_tuple():
    setstr = "{ (z1, z2) | z1 > 100 and z2 > 100 | z1 = x + y; z2 = x - y where x in SB_REALNUM_SET and y in SB_REALNUM_SET }"
    s = sb.build(setstr)
    g = s.choice(10)
    assert s and len(g) == 10 and all( z1 > 100 and z2 > 100 for (z1, z2) in g )

def test_booleanset():
    setstr = "{ z | True | z = x where x in SB_BOOLEAN_SET }"
    s = sb.build(setstr)
    g = s.choice(2)
    assert s and len(g) == 2
    tf = [ val for val in g ]
    assert tf[0] != tf[1]

def test_boolintset():
    setstr = "{ (z1, z2) | z2 > 0 | z1 = x; z2 = y where x in SB_BOOLEAN_SET and y in SB_INTNUM_SET }"
    s = sb.build(setstr)
    g = s.choice(10)
    assert s and len(g) == 10 and all( isinstance(z1, bool) and isinstance(z2, int) for (z1, z2) in g )

def test_set_complex():
    setstr = "{ x | True | x = z where z in SB_COMPLEXNUM_SET }"
    s = sb.build(setstr)
    g = s.choice(10)
    assert s and len(g) == 10 and all( isinstance(z, complex) for z in g )

def test_bracketsum():
    setstr = "{ z | z > 100 | z = max(x) where x in [SB_REALNUM_SET]*10 }"
    s = sb.build(setstr)
    g = s.choice(10)
    assert s and len(g) == 10 and all( (isinstance(z, int) or isinstance(z, float)) and z > 100 for z in g )

def test_ascii():
    setstr = "{ z | True | z = x[0] + x[1] + x[2] where x in [SB_ASCIIPRINTABLE_SET]*3 }"
    s = sb.build(setstr)
    g = s.choice(10)
    assert s and len(g) == 10 and all( ord(z[0]) >= 0 and ord(z[0]) < 128 for z in g )

def test_asciijoin():
    setstr = "{ z | True | z = ''.join(x) where x in [SB_ASCIIPRINTABLE_SET]*10 }"
    s = sb.build(setstr)
    g = s.choice(10)
    assert s and len(g) == 10 and all( len(z) == 10 for z in g )

def test_foreignchar():
    setstr = "{ y | True | y = u where u in { '하나', '둘', '셋' } }"
    s = sb.build(setstr)
    g = s.choice(3)
    assert s and len(g) == 3 and sorted(tuple(g)) == sorted(( '하나', '둘', '셋' ))

def test_setopor():
    setstr = "{ y | True | y = n where n in { 1, 2, 3 } | { 2, 3, 4 } }"
    s = sb.build(setstr)
    g = s.choice(4)
    assert s and len(g) == 4 and sorted(tuple(g)) == sorted(( 1, 2, 3, 4 ))

def test_setopand():
    setstr = "{ y | True | y = n where n in { 1, 2, 3 } & { 2, 3, 4 } }"
    s = sb.build(setstr)
    g = s.choice(2)
    assert s and len(g) == 2 and sorted(tuple(g)) == sorted(( 2, 3 ))

def test_setopsymmdiff():
    setstr = "{ y | True | y = n where n in { 1, 2, 3, 4 } ^ { 3, 4, 5, 6 } }"
    s = sb.build(setstr)
    g = s.choice(4)
    assert s and len(g) == 4 and sorted(tuple(g)) == sorted(( 1, 2, 5, 6 ))

def test_mixedset():
    setstr = "{ 0, y | True | y = n where n in { 1 }, 2}"
    s = sb.build(setstr)
    g = s.choice(3)
    assert s and len(g) == 3 and sorted(tuple(g)) == sorted(( 0, 1, 2 ))

def test_classindirect():
    setstr = "{ z | z.myvar > 10000 | z = MyClass(x) where x in SB_REALNUM_SET }"
    s1 = sb.build(setstr)

    setstr = "{ z | True | z = max(x[0].myvar,x[1].myvar) where x in [s1]*2 }"
    s2 = sb.build(setstr)

    g = s2.choice(10)
    assert s2 and len(g) == 10 and all( (isinstance(z, int) or isinstance(z, float)) and z > 10000 for z in g )

def test_classrange():
    varmin = 90000000.0
    varmax = 100000000.0
    setstr = "{ z | z.myvar > varmin and z.myvar < varmax | z = MyClass(x) where x in SB_FLOATNUM_SET }"
    s = sb.build(setstr)
    #import pdb; pdb.set_trace()
    g = s.choice(10)
    #print [ z.myvar for z in g ]
    assert s is not None and len(g) == 10 and all( isinstance(z, MyClass) and z.myvar > varmin and z.myvar < varmax for z in g )

def test_unique():
    varmin = 0.0
    varmax = 11.0
    #setstr = "{ z | z > varmin and z < varmax | z = x where x in SB_FLOATNUM_SET }"
    setstr = "{ z | z > varmin and z < varmax | z = x where x in SB_INTNUM_SET }"
    s = sb.build(setstr)
    #g = s.choice(10, unique=True, cache=False)
    g = s.choice(10, unique=True)
    #print [ z for z in g ]
    assert s is not None and len(g) == 10 and all( z > varmin and z < varmax for z in g )

def test_modulo():
    modulo = 2**10
    setstr = "{ z | z % modulo == 0 | z = x where x in SB_INTNUM_SET }"
    s = sb.build(setstr)
    #g = s.choice(10, unique=True, cache=False)
    g = s.choice(10, unique=True)
    #print [ z for z in g ]
    assert s is not None and len(g) == 10 and all( z%modulo == 0 for z in g )

