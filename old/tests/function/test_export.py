# -*- coding: UTF-8 -*-

"""setbuilder-python basic tests

"""

import setbuilder as sb

def test_twosets():
    LARGERTHANONE = lambda x: x > 1

    setstr1 = "{ a | LARGERTHANONE(a) | a = n where n in SB_NATURALNUM_SET }"
    s1 = sb.build(setstr1)

    setstr2 = "{ a | a > 10 | a = n where n in s1 }"
    s2 = sb.build(setstr2)

    g = s2.choice(2)

    assert s2 and len(g) == 2 and all( z > 10 for z in g )
    assert sb.export(s2, 'temp/twosets.py')

def test_import():
    imported = sb.load('temp/twosets.py')
    g = imported.choice(2)
    assert imported and len(g) == 2 and all( z > 10 for z in g )

def test_stdform():
    LARGERTHANONE = lambda x: x > 1

    setstr = "{ a | LARGERTHANONE(a) | a = n where n in SB_NATURALNUM_SET }"
    s = sb.build(setstr)
    sbid = s.sbid()

    setstr1 = "{ b | LARGERTHANONE(b) | b = k where k in SB_NATURALNUM_SET }"
    s1 = sb.build(setstr1)
    sbid1 = s1.sbid()
    assert sbid == sbid1

    setstr2 = "{ a | LARGERTHANONE  ( a ) | a  = n where n in SB_NATURALNUM_SET }"
    s2 = sb.build(setstr2)
    sbid2 = s2.sbid()
    assert sbid == sbid2

    # TODO: Support this
#    LARGERTHANONE1 = lambda x: x > 1
#    setstr3 = "{ b | LARGERTHANONE1(b) | b = k where k in SB_NATURALNUM_SET }"
#    s3 = sb.build(setstr3)
#    sbid3 = s3.sbid()
#    assert sbid == sbid3

