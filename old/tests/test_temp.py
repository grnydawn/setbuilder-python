# -*- coding: UTF-8 -*-

"""setbuilder-python basic tests

"""

import setbuilder as sb

class MyClass(object):

    def __init__(self, myvar):
        self.myvar = myvar

#def test_indexset():
#    setstr = "{ x | True | x[0]=1; x[1]=1; x[i+2] = x[i+1] + x[i]  where i in SB_INDEX_SET }"
#    s = sb.build(setstr)
#    #g = s.choice(10, unique=True, cache=False)
#    g = s.choice(10, unique=True)
#    print [ z for z in g ]
#    assert s is not None and [ x for x in g ] == [ 1, 1, 2, 3, 5, 8, 13, 21, 34, 55 ]


#def test_modulo():
#    modulo = 2**12 + 1
#    setstr = "{ z | z % modulo == 0 | z = x where x in SB_INTNUM_SET }"
#    s = sb.build(setstr)
#    #g = s.choice(10, unique=True, cache=False)
#    g = s.choice(10, unique=True)
#    #print [ z for z in g ]
#    assert s is not None and len(g) == 10 and all( z%modulo == 0 for z in g )

#def test_unique():
#    varmin = 1.0
#    varmax = 10.0
#    setstr = "{ z | z > varmin and z < varmax | z = x where x in SB_FLOATNUM_SET }"
#    s = sb.build(setstr)
#    #g = s.choice(10, unique=True, cache=False)
#    g = s.choice(10, unique=True)
#    print [ z for z in g ]
#    assert s is not None and len(g) == 10 and all( z > varmin and z < varmax for z in g )

#def test_class():
#    varmin = 90000000.0
#    varmax = 100000000.0
#    setstr = "{ z | z.myvar > varmin and z.myvar < varmax | z = MyClass(x) where x in SB_FLOATNUM_SET }"
#    s = sb.build(setstr)
#    g = s.choice(10)
#    #print [ z.myvar for z in g ]
#    assert s is not None and len(g) == 10 and all( isinstance(z, MyClass) and z.myvar > varmin and z.myvar < varmax for z in g )
