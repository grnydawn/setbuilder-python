# -*- coding: UTF-8 -*-

"""Frontend module for setbuilder-python

"""

import os
import inspect
import importlib
from setparser import parse
from basesets import Set
from config import SB_DEFAULT_TYPE, SB_EXPORT_TYPE, SB_SBID_TYPE
import ast
import shutil

def build(setnote, frozen=True, ordered=False):
    'Generates a set from a string'

    cf = inspect.currentframe().f_back

    return parse(setnote, cf.f_globals, cf.f_locals, frozen=frozen, ordered=ordered)

def load(instream):

    cf = inspect.currentframe().f_back

    if os.path.exists(instream):
        filecode = None
        with open(instream, 'r') as finstream:
            code_lines = finstream.read().split('\n')
            for prev_line, next_line in zip(code_lines[:-1], code_lines[1:]):
                if next_line.startswith('SB_EXPORTED_SET') and len(prev_line) == 65:
                    filecode = prev_line[1:13]
                    break
        sbsetfile = '%s/.setbuilder/set/%s.py'%(os.path.expanduser('~'), filecode)
        if filecode:
            if not os.path.exists(sbsetfile):
                shutil.copyfile(instream, sbsetfile)

            mod = __import__(filecode, cf.f_globals, cf.f_locals)
            mod.SB_EXPORTED_SET.import_env = {}
            for name in dir(mod):
                if name not in ['setbuilder', 'finstream', 'code_obj', 'instream', 'SB_EXPORTED_SET']:
                    mod.SB_EXPORTED_SET.import_env[name] = getattr(mod, name)
            mod.SB_EXPORTED_SET.__frozen__ = True
            return mod.SB_EXPORTED_SET
    else:
        raise Exception('File does not exist: %s'%instream)

def export(sbset, outstream):
    'Exports a set'

    cache = []

    def DFSWrite(f, out, depth=0):
        if out.has_key('uses'):
            for used in out['uses']:
                DFSWrite(f, used, depth=depth+1)
        if out.has_key('content'):
            lines = []
            if out.has_key('sbid'):
                lines.append('#%s'%out['sbid'])
            if depth == 0:
                lines.append('SB_EXPORTED_SET = setbuilder.build("%s", frozen=False)'%out['content'])
            else:
                lines.append(out['content'])
            outstr = '\n'.join(lines)
            if outstr not in cache:
                f.write(outstr+'\n\n')

    dirname, filename = os.path.split(outstream)
    if os.path.exists(dirname):

        cf = inspect.currentframe().f_back

        opts = {}
        opts['complete'] = True
        opts['sbid'] = True
        opts['reprtype'] = SB_EXPORT_TYPE
        opts['raise'] = True

        out = sbset._sb___repr__(cf.f_globals, cf.f_locals, **opts)

        #import pdb; pdb.set_trace()
        with open(outstream, 'w') as f:
            # add python version and setbuilder version
            f.write('# -*- coding: UTF-8 -*-\n\n')
            f.write('import setbuilder\n\n')
            if isinstance(out, dict):
                DFSWrite(f, out)
                return True
        return False
    else:
        raise Exception('%s does not exist.'%os.path.dirname(outstream))

def powerset(s):
    'Generates a power set'
    pass
