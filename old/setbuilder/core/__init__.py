"""classes module

"""

import os
import sys

HOME = os.path.expanduser('~')
SBHOME = '%s/.setbuilder'%HOME
SBSET = '%s/set'%SBHOME
SBCACHE = '%s/cache'%SBHOME
SBCHOICE = '%s/choice'%SBHOME

if not os.path.exists(SBHOME):
    os.mkdir(SBHOME)

if not os.path.exists(SBSET):
    os.mkdir(SBSET)
open('%s/__init__'%SBSET, 'w').close()
sys.path.append(SBSET)

if not os.path.exists(SBCACHE):
    os.mkdir(SBCACHE)
open('%s/__init__'%SBCACHE, 'w').close()
sys.path.append(SBCACHE)

if not os.path.exists(SBCHOICE):
    os.mkdir(SBCHOICE)
open('%s/__init__'%SBCHOICE, 'w').close()
sys.path.append(SBCHOICE)

del HOME, SBHOME, SBSET, SBCACHE, SBCHOICE

from basesets import Set, EnumSet, PredicateSet, CompoundSet, SB_INDEX_SET
from choice import Choice, SB_LARGER_CHOICE, SB_ACCEPTED_CHOICE, SB_SMALLER_CHOICE, SB_DUPLICATE_CHOICE
from functions import build, export, load
from builtinsets import *
