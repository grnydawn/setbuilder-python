'''Configuration
'''

import sys

###########################################################################
# Values
###########################################################################

SB_MAXINT_VALUE         = sys.maxint if 'maxint' in dir(sys) else sys.maxsize
SB_MININT_VALUE         = -sys.maxint-1 if 'maxint' in dir(sys) else -sys.maxsize-1
SB_MAXFLOAT_VALUE       = sys.float_info.max
SB_MINFLOAT_VALUE       = sys.float_info.min


###########################################################################
# Types
###########################################################################

SB_DEFAULT_TYPE         = 0
SB_EXPORT_TYPE          = 1
SB_STDFORM_TYPE         = 2
SB_SBID_TYPE            = 3

