# -*- coding: UTF-8 -*-

"""Set classes
"""

import os
import sys
import hashlib
import random
import inspect
import itertools
import source
import choice
import operator
import functools
import astunparse
import time
from collections import OrderedDict
from types import ModuleType
from config import SB_DEFAULT_TYPE, SB_EXPORT_TYPE, SB_SBID_TYPE, SB_STDFORM_TYPE, \
    SB_MAXINT_VALUE

# TODO: Python 3 and unicode support
# TODO: Docstring and documentation
# TODO: continuous testing setup with github
# TODO: set choice cache can be reconstructed through using numbers and exported sets

###########################################################################
# Common
###########################################################################

MAX_COUNT_CHOICE    = 100
MAX_COUNT_SAMESIZE  = 10

def _get_set(name, gids, lids):
    sbset = name

    if lids and sbset in lids:
        if inspect.isclass(lids[sbset]) and issubclass(lids[sbset], Set):
            return lids[sbset](gids=gids, lids=lids)
        else:
            return lids[sbset]

    if gids and sbset in gids:
        if inspect.isclass(gids[sbset]) and issubclass(gids[sbset], Set):
            return gids[sbset](gids=gids, lids=lids)
        else:
            return gids[sbset]

    if isinstance(name, str):
        if name == 'SB_INDEX_SET':
            return SB_INDEX_SET()

        import builtinsets as bis
        if name in dir(bis):
            sbset = getattr(bis, name, name)
        if inspect.isclass(sbset) and issubclass(sbset, PrimitiveSet):
            sbset = sbset()
    return sbset

###########################################################################
# Base sets
###########################################################################

class Set(object):
    """Base class for Set-like classes
    """
    __sets__ = {}

    def __new__(cls, *args, **opts):
        return object.__new__(cls, *args, **opts)

    def __init__(self, **opts):
        '''This constructor should be called at the last by inherited classes.

        '''
        self.__sets__[self] = None
        self.gids = opts.get('gids', None)
        self.lids = opts.get('lids', None)

    def _sb_defer(self, caller, *args, **opts):
        from errors import handle_exception

        cf = inspect.currentframe().f_back.f_back

        try:
            return getattr(self, '_sb_%s'%caller)(cf.f_globals, cf.f_locals, *args, **opts)
        except Exception as e:
            handle_exception(e)

    #def __hash__(self):
    #    return int (self.setid[-8:], 16)

    def __str__(self):
        return self._sb_defer('__str__')

    def __setattr__(self, name, value):
        if getattr(self, '__frozen__', False):
            raise Exception("Can't modify frozen set")
        else:
            super(Set, self).__setattr__(name, value)

    def __delattr__(self, name):
        if getattr(self, '__frozen__', False):
            raise Exception("Can't modify frozen set")
        else:
            super(Set, self).__delattr__(name)

    def _sb_isbuiltindata(self, gids, lids, elem, **opts):

        try:
            for k, v in elem.items():
                if not self._sb_isbuiltindata(k): return False
                if not self._sb_isbuiltindata(v): return False
        except:
            try:
                for e in elem:
                    if not self._sb_isbuiltindata(e): return False
            except:
                if type(elem) in [ bool, int, float, long, complex, str]:
                    return True
                else:
                    return False
        return True

    def _sb___repr__(self, gids, lids, **opts):
        out = {}
        out['content'] = self._sb___str__(gids, lids, **opts)
        return out

    def _sb_stdform(self, gids, lids, **opts):
        prev_reprtype = opts.get('reprtype', None)
        opts['reprtype'] = SB_STDFORM_TYPE
        out = self._sb___repr__(gids, lids, **opts)
        if prev_reprtype:
            opts['reprtype'] = prev_reprtype
        return out['content']

    def _sb_sbid(self, gids, lids, **opts):
        prev_complete = opts.get('complete', None)
        prev_reprtype = opts.get('reprtype', None)
        opts['complete'] = True
        opts['reprtype'] = SB_SBID_TYPE
        out = self._sb___repr__(gids, lids, **opts)
        if prev_complete:
            opts['complete'] = prev_complete
        if prev_reprtype:
            opts['reprtype'] = prev_reprtype
        if opts.get('shortform', False):
            return out['content'][:12]
        else:
            return out['content']

    def __contains__(self, elem):
        return self._sb_defer('__contains__', elem)

    def __or__(self, other):
        return self._sb_defer('__or__', other)

    def __and__(self, other):
        return self._sb_defer('__and__', other)

    def __sub__(self, other):
        return self._sb_defer('__sub__', other)

    def __xor__(self, other):
        return self._sb_defer('__xor__', other)

    def _sb___or__(self, gids, lids, other):

        if isinstance(other, Set):
            indexed = getattr(self, '__indexed__', False) and getattr(other, '__indexed__', False)
            frozen = getattr(self, '__frozen__', True) or getattr(other, '__frozen__', True)
            return CompoundSet(self, '|', other, frozen=frozen, indexed=indexed)
        else:
            indexed = getattr(self, '__indexed__', False)
            frozen = getattr(self, '__frozen__', True)
            return CompoundSet(self, '|', EnumSet(other), frozen=frozen, indexed=indexed)

    def _sb___and__(self, gids, lids, other):

        if isinstance(other, Set):
            indexed = getattr(self, '__indexed__', False) and getattr(other, '__indexed__', False)
            frozen = getattr(self, '__frozen__', True) or getattr(other, '__frozen__', True)
            return CompoundSet(self, '&', other, frozen=frozen, indexed=indexed)
        else:
            indexed = getattr(self, '__indexed__', False)
            frozen = getattr(self, '__frozen__', True)
            return CompoundSet(self, '&', EnumSet(other), frozen=frozen, indexed=indexed)

    def _sb___sub__(self, gids, lids, other):

        if isinstance(other, Set):
            indexed = getattr(self, '__indexed__', False) and getattr(other, '__indexed__', False)
            frozen = getattr(self, '__frozen__', True) or getattr(other, '__frozen__', True)
            return CompoundSet(self, '-', other, frozen=frozen, indexed=indexed)
        else:
            indexed = getattr(self, '__indexed__', False)
            frozen = getattr(self, '__frozen__', True)
            return CompoundSet(self, '-', EnumSet(other), frozen=frozen, indexed=indexed)

    def _sb___xor__(self, gids, lids, other):

        if isinstance(other, Set):
            indexed = getattr(self, '__indexed__', False) and getattr(other, '__indexed__', False)
            frozen = getattr(self, '__frozen__', True) or getattr(other, '__frozen__', True)
            return CompoundSet(self, '^', other, frozen=frozen, indexed=indexed)
        else:
            indexed = getattr(self, '__indexed__', False)
            frozen = getattr(self, '__frozen__', True)
            return CompoundSet(self, '^', EnumSet(other), frozen=frozen, indexed=indexed)

    def __iter__(self):
        return self._sb_defer('__iter__')

    def choice(self, size, **opts):

        if size < 1:
            return EnumSet([], frozen=opts.get('frozen', True), indexed=True)

        # used in Tester and Generator
        prev_import_env = opts.get('import_env', None)
        if hasattr(self, 'import_env'):
            opts['import_env'] = self.import_env

        if opts.has_key('timeout') and opts['timeout'] >= 0:
            opts['timeout'] = -(time.clock() + opts['timeout'])

        elems = []
        callback = lambda result: True
        for _ in xrange(MAX_COUNT_CHOICE*size):

            if opts.has_key('timeout') and opts['timeout'] + time.clock() > 0: break

            opts['choice'] = {}
            opts['choice']['path'] = []

            elem, callback = self._sb_defer('choice', **opts)
            if elem is not None:
                if opts.get('unique', True):
                    if elem in elems:
                        callback(choice.SB_DUPLICATE_CHOICE)
                    else:
                        elems.append(elem)
                        callback(choice.SB_ACCEPTED_CHOICE)
                else:
                    elems.append(elem)
            else:
                pass

            if len(elems) >= size:
                break

        callback(choice.SB_END_CHOICE)

        if prev_import_env:
            opts['import_env'] = prev_import_env

        return EnumSet(elems, frozen=opts.get('frozen', True), indexed=True)

    def add(self, elem, **opts):
        return self._sb_defer('add', elem, **opts)

    def discard(self, elem, **opts):
        return self._sb_defer('discard', elem, **opts)

    def remove(self, elem, **opts):
        return self._sb_defer('remove', elem, **opts)

    def update(self, elems, **opts):
        return self._sb_defer('update', elems, **opts)

    def stdform(self):
        return self._sb_defer('stdform')

#    def sbidform(self):
#        '''{ sbvarid | sbtestid | sbgenid where sbvarid in sbsetid }
#            sbvarid : SB_len_SVAR, WVAR
#            sbtestid: SB_sbid_TEST
#            sbgenid : SB_sbid_GEN
#            sbsetid : SB_sbid_SET
#
#            sbexportidd : SB_sbid_SAVE
#
#        '''
#        return self._sb_defer('sbidform')

    def sbid(self, shortform=False):
        '''256bits'''
        return self._sb_defer('sbid', shortform=shortform)

class SB_INDEX_SET(Set):

    def __init__(self, **opts):

        self.__indexed__ = True
        self.default_start = 0
        self.next_index = self.default_start

        super(SB_INDEX_SET, self).__init__(**opts)

        self.__frozen__ = True

    def _sb___contains__(self, gids, lids, elem, **opts):
        return type(elem) == type(1) and elem >= 0

    def _sb_choice(self, gids, lids, **opts):
        return  (self.next_index, self.next_index), self._sb_update

    def _sb_update(self, result, elem, retval):
        pass

    def _sb___str__(self, gids, lids, **opts):
        return self.__class__.__name__

    def _sb___repr__(self, gids, lids, **opts):

        out = {}
        if opts.get('reprtype', SB_DEFAULT_TYPE) == SB_SBID_TYPE:
            out['content'] = hashlib.sha256(self.__class__.__name__).hexdigest()
        else:
            out['content'] = self.__class__.__name__
            if opts.get('reprtype', SB_DEFAULT_TYPE) == SB_EXPORT_TYPE:
                out['sbid'] = hashlib.sha256(self.__class__.__name__).hexdigest()
        return out

class PrimitiveSet(Set):
    '''Primitive set that does not derived from other set

    '''

    _amap = {}
    _cmap = {}

    def __init__(self, **opts):

        # config elem generation logics per prim type
        self.cache = {}
        self.__indexed__ = True

        super(PrimitiveSet, self).__init__(**opts)

        self.__frozen__ = True

    def _sb_cacheupdate(self, elem, algo):
        def newfunc(result):
            if (type(result) == type(1) and result == choice.SB_ACCEPTED_CHOICE) or \
                (type(result) == type(True) and result) or \
                (type(result)== type(1.0) and result == 0.0):
                if algo.hits < SB_MAXINT_VALUE:
                    algo.hits += 1
                algo.choicedb['cacheblockcount'] = max(algo.choicedb['cacheblockcount'] - 1, -10)
            else:
                if algo.misses < SB_MAXINT_VALUE:
                    algo.misses += 1
                algo.choicedb['cacheblockcount'] += 2

                if result == choice.SB_DUPLICATE_CHOICE:
                    if elem not in algo.choicedb['dupulicated']:
                        algo.choicedb['dupulicated'].append(elem)
                elif result == choice.SB_END_CHOICE:
                    algo.choicedb['dupulicated'] = []
                else:
                    if elem in algo.choicedb['cache']:
                        algo.choicedb['cache'].remove(elem)

        return newfunc

    def _sb_algoupdate(self, func, elem, retval, algo):
        def newfunc(result):
            if isinstance(result, bool):
                if result:
                    if elem is not None and elem not in algo.choicedb['cache']:
                        algo.choicedb['cache'].append(elem)
                    if algo.hits < SB_MAXINT_VALUE:
                        algo.hits += 1
                else:
                    if algo.misses < SB_MAXINT_VALUE:
                        algo.misses += 1
            elif isinstance(result, int):
                if result == choice.SB_ACCEPTED_CHOICE:
                    if elem is not None and elem not in algo.choicedb['cache']:
                        algo.choicedb['cache'].append(elem)
                    if algo.hits < SB_MAXINT_VALUE:
                        algo.hits += 1
                elif result == choice.SB_END_CHOICE:
                    algo.choicedb['dupulicated'] = []
                else:
                    if algo.misses < SB_MAXINT_VALUE:
                        algo.misses += 1
            elif isinstance(result, float):
                pass
            else:
                raise Exception('Not supported result type: %s'%str(result))

            func(result, elem, retval)

        return newfunc

    def _sb_genelems(self, gids, lids, choicepath, cache):

        pathcode = hashlib.sha256(''.join(choicepath)).hexdigest()[:12]

        if not self._cmap.has_key(pathcode):
            self._cmap[pathcode] = []

        algos = []
        if not self._amap.has_key(pathcode):
            sbalgofile = '%s/.setbuilder/choice/A%s.py'%(os.path.expanduser('~'), pathcode)
            if not os.path.exists(sbalgofile):
                # put all algorithms in the algofile
                srclines = []
                # TODO: allow indirect subclasses??
                for i, subcls in enumerate(choice.Choice.__subclasses__()):
                    clsname = subcls.__name__
                    srcline = 'from setbuilder import %s\nalgorithm_%d = %s(%s, %s, %s)'
                    srclines.append(srcline%(clsname, i, clsname, str(self._BITLEN), str(self._MINVAL), str(self._MAXVAL)))
                    srclines.append('algorithm_%d.hits = 1'%i)
                    srclines.append('algorithm_%d.misses = 1'%i)
                    srclines.append('\n')

                with open(sbalgofile, 'w') as f:
                    f.write('\n'.join(srclines))

            mod = __import__('A%s'%pathcode, gids, lids)
            for name in dir(mod):
                if name.startswith('algorithm_'):
                    algoobj = getattr(mod, name)
                    algoobj.choicedb['cache'] = self._cmap[pathcode]
                    algoobj.choicedb['cacheblockcount'] = 0
                    algoobj.choicedb['dupulicated'] = []
                    algos.append(algoobj)

            self._amap[pathcode] = algos
        else:
            algos = self._amap[pathcode]

        # select a algo
        algo = choice.select(algos)

        # use cache
        #if cache and algo.choicedb['cacheblockcount'] <= 0 and len(algo.choicedb['cache']) > 0:
        if cache and algo.choicedb['cacheblockcount'] <= 0 and \
            len(algo.choicedb['cache']) - len(algo.choicedb['dupulicated'])> 0:
            for _ in range(10):
                elem = random.choice(algo.choicedb['cache'])
                if elem is not None and elem not in algo.choicedb['dupulicated']:
                    return elem, self._sb_cacheupdate(elem, algo)

        # generate elems
        (elem, retval), callback = algo.generate()

        #return elem, callback
        return elem, self._sb_algoupdate(callback, elem, retval, algo)

    def _sb___str__(self, gids, lids, **opts):
        return self.__class__.__name__


    def _sb___repr__(self, gids, lids, **opts):

        out = {}
        if opts.get('reprtype', SB_DEFAULT_TYPE) == SB_SBID_TYPE:
            out['content'] = hashlib.sha256(self.__class__.__name__).hexdigest()
        else:
            out['content'] = self.__class__.__name__
            if opts.get('reprtype', SB_DEFAULT_TYPE) == SB_EXPORT_TYPE:
                out['sbid'] = hashlib.sha256(self.__class__.__name__).hexdigest()
        return out

###########################################################################
# Enum set
###########################################################################

class EnumSet(Set):
    '''enumerative set

        Cases for EnumSet creation
        --------------------------
        1. EnumSet(iterable) or EnumSet(EnumSet)
        2. setbuilder.build({1,2,3})
        3. EnumSet setop Enumset
        4. setbuilder.choice()

        Parameters for EnumSet creation
        -------------------------------
        frozen=[True|False]

        Default data type for EnumSet elems
        -----------------------------------
        dictionary: unindexed
        dictionary with key sorted: indexed, user should provides a way to check order


    '''

    def __init__(self, *args, **opts):
        self.__indexed__ =  opts.pop('indexed', True)
        if self.__indexed__:
            self.elems = OrderedDict()
        else:
            self.elems = dict()

        if len(args) > 0:
            for arg in args:
                if isinstance(arg, EnumSet):
                    for elem, attrs in arg.elems.items():
                        self.elems[elem] = attrs
                elif isinstance(arg, str):
                    self.elems[_get_set(arg, opts.get('gids', None), opts.get('lids', None))] = None
                else:
                    for elem in arg:
                        self.elems[elem] = None

        super(EnumSet, self).__init__(**opts)

        self.__frozen__ = opts.pop('frozen', True)

    def _sb_add(self, gids, lids, elem, attrs=None, **opts):
        self.elems[elem] = attrs

    def _sb_discard(self, gids, lids, elem, **opts):
        if elem in self.elems:
            del self.elems[elem]

    def _sb_remove(self, gids, lids, elem, **opts):
        del self.elems[elem]

    def _sb_update(self, gids, lids, elems, **opts):
        for elem in elems:
            self._sb_add(gids, lids, elem, **opts)

    def __eq__(self, other):
        return self._sb_defer('__eq__', other)

    def _sb___eq__(self, gids, lids, other):
        for elem in self:
            if elem not in other:
                return False
        return True

    def _sb___iter__(self, gids, lids):
        for elem in self.elems: yield elem

    def __len__(self):
        return self._sb_defer('__len__')

    def _sb___len__(self, gids, lids, **opts):
        return len(self.elems)

    def _sb___contains__(self, gids, lids, elem, **opts):
        return elem in self.elems.keys()

    def __getitem__(self, index):
        if not getattr(self, '__indexed__', False):
            raise Exception("This is not an indexed set.")
        else:
            return self.elems.keys()[index]

    def _sb___or__(self, gids, lids, other):

        if isinstance(other, Set):
            indexed = getattr(self, '__indexed__', False) and getattr(other, '__indexed__', False)
            frozen = getattr(self, '__frozen__', True) or getattr(other, '__frozen__', True)
            if isinstance(other, EnumSet):
                s = EnumSet(self, other, frozen=frozen, indexed=indexed)
            else:
                s = MixedSet(self, other, frozen=frozen, indexed=indexed)
        else:
            s = EnumSet(self, other, frozen=getattr(self, '__frozen__', True), indexed=True)

        return s

    def __ior__(self, other):
        return self._sb_defer('__ior__', other)

    def _sb___ior__(self, gids, lids, other):
        if self.__frozen__:
            raise Exception('Frozen set can not be modified.')

        retobj = self
        if isinstance(other, Set):
            indexed = getattr(self, '__indexed__', False) and getattr(other, '__indexed__', False)
            if isinstance(other, EnumSet):
                self._sb_update(gids, lids, other)
                self.__indexed__ = indexed
            else:
                retobj = MixedSet(self, other, frozen=False, indexed=indexed)
        else:
            for elem in other:
                self._sb_add(elem)

        return retobj

    def _sb___and__(self, gids, lids, other):

        if isinstance(other, Set):
            indexed = getattr(self, '__indexed__', False) and getattr(other, '__indexed__', False)
            frozen = getattr(self, '__frozen__', True) or getattr(other, '__frozen__', True)
        else:
            indexed = getattr(self, '__indexed__', False)
            frozen = getattr(self, '__frozen__', True)

        s = EnumSet(frozen=False, indexed=indexed)
        for elem in self:
            if elem in other:
                s._sb_add(gids, lids, elem)
        s.__indexed__ = indexed
        s.__frozen__ = frozen
        return s

    def __iand__(self, other):
        return self._sb_defer('__iand__', other)

    def _sb___iand__(self, gids, lids, other):
        if self.__frozen__:
            raise Exception('Frozen set can not be modified.')

        if isinstance(other, Set):
            indexed = getattr(self, '__indexed__', False) and getattr(other, '__indexed__', False)
        else:
            indexed = getattr(self, '__indexed__', False)

        if indexed:
            elems = OrderedDict()
        else:
            elems = dict()

        for e, a in self.elems.items():
            if e in other:
                elems[e] = a
        self.elems = elems
        self.__indexed__ = indexed

        return self

    def _sb___sub__(self, gids, lids, other):

        indexed = getattr(self, '__indexed__', False)
        frozen = getattr(self, '__frozen__', True)

        s = EnumSet(frozen=False, indexed=indexed)
        for elem in self:
            if elem not in other:
                s._sb_add(gids, lids, elem)
        s.__indexed__ = indexed
        s.__frozen__ = frozen
        return s

    def __isub__(self, other):
        return self._sb_defer('__isub__', other)

    def _sb___isub__(self, gids, lids, other):
        if self.__frozen__:
            raise Exception('Frozen set can not be modified.')

        if isinstance(other, Set):
            indexed = getattr(self, '__indexed__', False) and getattr(other, '__indexed__', False)
        else:
            indexed = getattr(self, '__indexed__', False)

        if indexed:
            elems = OrderedDict()
        else:
            elems = dict()
        for e, a in self.elems.items():
            if e not in other:
                elems[e] = a
        self.elems = elems
        self.__indexed__ = indexed

        return self

    def _sb___xor__(self, gids, lids, other):

        if isinstance(other, Set):
            indexed = getattr(self, '__indexed__', False) and getattr(other, '__indexed__', False)
            frozen = getattr(self, '__frozen__', True) or getattr(other, '__frozen__', True)

            if isinstance(other, EnumSet):
                s = EnumSet(self, frozen=False, indexed=indexed)
                for elem in other:
                    if elem in self:
                        s._sb_remove(gids, lids, elem)
                    else:
                        s._sb_add(gids, lids, elem)
            else:
                u = self | other
                v = self & other
                s = u - v
        else:
            indexed = getattr(self, '__indexed__', False)
            frozen = getattr(self, '__frozen__', True)
            s = EnumSet(self, frozen=False, indexed=indexed)
            for elem in other:
                if elem in s:
                    s._sb_remove(gids, lids, elem)
                else:
                    s._sb_add(gids, lids, elem)
        s.__indexed__ = indexed
        s.__frozen__ = frozen
        return s

    def __ixor__(self, other):
        return self._sb_defer('__ixor__', other)

    def _sb___ixor__(self, gids, lids, other):
        if self.__frozen__:
            raise Exception('Frozen set can not be modified.')

        if isinstance(other, Set):
            indexed = getattr(self, '__indexed__', False) and getattr(other, '__indexed__', False)

            if isinstance(other, EnumSet):
                for elem in other:
                    if elem in self:
                        self._sb_remove(gids, lids, elem)
                    else:
                        self._sb_add(gids, lids, elem)
            else:
                u = self | other
                v = self & other
                self = u - v
        else:
            indexed = getattr(self, '__indexed__', False)
            for elem in other:
                if elem in self:
                    self._sb_remove(gids, lids, elem)
                else:
                    self._sb_add(gids, lids, elem)
        self.__indexed__ = indexed

        return self

    def _sb_choice(self, gids, lids, **opts):

        '''Generate elements
        NOTE: all elements in SB set should work with "==" and "!=" operator
        b = copy.deepcopy(a) ; a == b?? during testing or inspect class definition during generation
        '''

        if len(self.elems) == 0:
            return None

        return random.choice(self.elems.keys()), lambda result: True

    def _sb___repr__(self, gids, lids, **opts):
        '''Exports elements

        Exportable objects
        ------------------
        - built-in data types
        - exportable setbuilder sets

        Notes
        -----

        add standardform:True into opts
        expects function or lambda:{} if other lambda, function is required
        '''

        l = []
        u = []

        for e in self.elems:
            if isinstance(e, Set):
                o = e._sb___repr__(gids, lids, **opts)
                l.append(o['content'])
                u.extend(o.get('uses', []))
            else:
                if self._sb_isbuiltindata(gids, lids, e, **opts):
                    l.append(str(e))
                else:
                    raise Exception('Non-builtin data type can not be exported.')

        content = '{ %s }'%', '.join(l)

        out = {}
        if opts.get('reprtype', SB_DEFAULT_TYPE) == SB_SBID_TYPE:
            out['content'] = hashlib.sha256(content).hexdigest()
        else:
            out['content'] = content
            if opts.get('reprtype', SB_DEFAULT_TYPE) == SB_EXPORT_TYPE:
                out['sbid'] = hashlib.sha256(content).hexdigest()
        if opts.get('complete', False):
            out['uses'] = u
        return out

    def _sb___str__(self, gids, lids, **opts):
        l = []
        for e in self.elems:
            if isinstance(e, Set):
                l.append(e._sb___str__(gids,lids, **opts))
            else:
                l.append(str(e))
        return '{ %s }'%', '.join(l)

###########################################################################
# Compound  set
###########################################################################

class CompoundSet(Set):
    def __init__(self, left, op, right, **opts):

        if isinstance(left, str):
            self.left = _get_set(left, opts.get('gids', None), opts.get('lids', None))
        else:
            self.left = left

        self.op = op

        if isinstance(right, str):
            self.right = _get_set(right, opts.get('gids', None), opts.get('lids', None))
        else:
            self.right = right

        self.paren = False
        super(CompoundSet, self).__init__(**opts)

    def _sb___contains__(self, gids, lids, elem, **opts):

        if self.left is None or self.right is None:
            raise Excpetion("One or both of set classes are None")

        if self.op == '|':
            if self.left._sb___contains__(gids, lids, elem, **opts) or \
                self.right._sb___contains__(gids, lids, elem, **opts):
                return True
        elif self.op == '&':
            if self.left._sb___contains__(gids, lids, elem, **opts) and \
                self.right._sb___contains__(gids, lids, elem, **opts):
                return True
        elif self.op == '-':
            if self.left._sb___contains__(gids, lids, elem, **opts) and \
                not self.right._sb___contains__(gids, lids, elem, **opts):
                return True
        elif self.op == '^':
            if ( self.left._sb___contains__(gids, lids, elem, **opts) or \
                self.right._sb___contains__(gids, lids, elem, **opts) ) and \
                not ( self.left._sb___contains__(gids, lids, elem, **opts) and \
                self.right._sb___contains__(gids, lids, elem, **opts) ) :
                return True

        return False

    def _sb_choice(self, gids, lids, **opts):

        '''Generate elements

        NOTE: all elements in SB set should work with "==" and "!=" operator
        b = copy.deepcopy(a) ; a == b?? during testing or inspect class definition during generation
        '''

        prevpath = opts['choice']['path'][:]
        mysbid = self._sb_sbid(gids, lids, **opts)

        lcache = []
        rcache = []

        for _ in range(MAX_COUNT_CHOICE):

            if opts.has_key('timeout') and opts['timeout'] + time.clock() > 0: break

            opts['choice']['path'].append(self._sb_sbid(gids, lids, **opts))

            retelem = None
            callbacks = [ lambda result: True, lambda result: True]
            oddeven = int(time.time()*1000000) % 2

            try:
                if self.op == '|':
                    if oddeven == 0:
                        opts['choice']['path'] += ['L']
                        retelem, callback = self.left._sb_choice(gids, lids, **opts)
                        callbacks[0] = callback
                    else:
                        opts['choice']['path'] += ['R']
                        retelem, callback = self.right._sb_choice(gids, lids, **opts)
                        callbacks[1] = callback

                    if retelem is None:
                        callback(False)
                    elif opts['choice']['path'][0] == mysbid:
                        callback(True)

                elif self.op == '&':
                    if oddeven == 0:
                        choiceset = self.left
                        checkset = self.right
                        opts['choice']['path'] += ['L']
                    else:
                        choiceset = self.right
                        checkset = self.left
                        opts['choice']['path'] += ['R']

                    elem, callback = choiceset._sb_choice(gids, lids, **opts)
                    if elem is None:
                        callback(False)
                    elif checkset._sb___contains__(gids, lids, elem, **opts):
                        retelem = elem
                        callbacks[oddeven] = callback
                        if opts['choice']['path'][0] == mysbid:
                            callback(True)
                    else:
                        callback(False)

                elif self.op == '-':

                    opts['choice']['path'] += ['L']
                    elem, callback = self.left._sb_choice(gids, lids, **opts)

                    if elem is None:
                        callback(False)
                    elif self.right._sb___contains__(gids, lids, elem, **opts):
                        callback(False)
                    else:
                        retelem = elem
                        callbacks[0] = callback
                        if opts['choice']['path'][0] == mysbid:
                            callback(True)

                elif self.op == '^':
                    if oddeven == 0:
                        choiceset = self.left
                        checkset = self.right
                        opts['choice']['path'] += ['L']
                    else:
                        choiceset = self.right
                        checkset = self.left
                        opts['choice']['path'] += ['R']

                    elem, callback = choiceset._sb_choice(gids, lids, **opts)
                    if elem is None:
                        callback(False)
                    elif checkset._sb___contains__(gids, lids, elem, **opts):
                        callback(False)
                    else:
                        retelem = elem
                        callbacks[oddeven] = callback
                        if opts['choice']['path'][0] == mysbid:
                            callback(True)

            except Exception as e:
                raise

            if retelem is not None:
                break

            opts['choice']['path'] = []

        opts['choice']['path'] = prevpath

        return retelem, lambda result: map(operator.methodcaller('__call__', result), callbacks)

    def _sb___str__(self, gids, lids, **opts):
        if isinstance(self.left, Set):
            lstr = self.left._sb___str__(gids, lids, **opts)
        else:
            lstr = str(self.left)

        opstr = str(self.op)

        if isinstance(self.right, Set):
            rstr = self.right._sb___str__(gids, lids, **opts)
        else:
            rstr = str(self.right)

        if self.paren:
            return '( %s %s %s )'%(lstr, opstr, rstr)
        else:
            return '%s %s %s'%(lstr, opstr, rstr)

    def _sb___repr__(self, gids, lids, **opts):

        if isinstance(self.left, Set):
            lout = self.left._sb___repr__(gids, lids, **opts)
        else:
            raise Exception('Not a set object.')

        opstr = str(self.op)

        if isinstance(self.right, Set):
            rout = self.right._sb___repr__(gids, lids, **opts)
        else:
            raise Exception('Not a set object.')
            #rout = str(self.right)

        content = '( %s %s %s )'%(lout['content'], opstr, rout['content'])

        out = {}
        if opts.get('reprtype', SB_DEFAULT_TYPE) == SB_SBID_TYPE:
            out['content'] = hashlib.sha256(content).hexdigest()
        else:
            out['content'] = content
            if opts.get('reprtype', SB_DEFAULT_TYPE) == SB_EXPORT_TYPE:
                out['sbid'] = hashlib.sha256(content).hexdigest()
        if opts.get('complete', False):
            out['uses'] = lout.get('uses', []) + rout.get('uses', [])
        return out


    def tree(self, depth=0):
        l = []

        l.append('    '*depth + '%s: '%self.__class__.__name__ + self._sb___str__(gids, lids, **opts))

        for elem in [ self.left, self.op, self.right ]:
            if hasattr(elem, 'tree'):
                l.append('    '*(depth+1) + elem.tree(depth=depth+1))
            else:
                if isinstance(elem, Set):
                    l.append('    '*(depth+1) + elem._sb___str__(gids, lids, **opts))
                else:
                    l.append('    '*(depth+1) + set(elem))

        return '\n'.join(l)

###########################################################################
# Predicate  set
###########################################################################

class PredicateSet(Set):

    def __init__(self, setrule, **opts):

        self.setrule = setrule
        super(PredicateSet, self).__init__(**opts)

    def _sb___len__(self, gids, lids, **opts):
        return len(self.setrule)

    def _sb___contains__(self, gids, lids, elem, **opts):
        return self.setrule.contains(gids, lids, elem, **opts)

    def _sb___str__(self, gids, lids, **opts):
        return '{ %s }'%self.setrule._sb___str__(gids, lids, **opts)

    def _sb_choice(self, gids, lids, **opts):
        sbid = self._sb_sbid(gids, lids, **opts)
        return self.setrule._sb_choice(gids, lids, sbid, **opts)

    def _sb___repr__(self, gids, lids, **opts):

        rule, uses = self.setrule._sb___repr__(gids, lids, **opts)

        out = {}
        content = '{ %s }'%rule
        if opts.get('reprtype', SB_DEFAULT_TYPE) == SB_SBID_TYPE:
            out['content'] = hashlib.sha256(content).hexdigest()
        else:
            out['content'] = content
            if opts.get('reprtype', SB_DEFAULT_TYPE) == SB_EXPORT_TYPE:
                out['sbid'] = hashlib.sha256(content).hexdigest()

        if opts.get('complete', False):
            out['uses'] = uses
        return out

    def tree(self, depth=0):
        return '    '*depth + '%s: '%self.__class__.__name__ + self._sb___str__(gids, lids, **opts)

class PredicateBase(object):

    def __setattr__(self, name, value):
        if getattr(self, '__frozen__', False):
            raise Exception("Can't modify frozen set")
        else:
            super(PredicateBase, self).__setattr__(name, value)

    def __delattr__(self, name):
        if getattr(self, '__frozen__', False):
            raise Exception("Can't modify frozen set")
        else:
            super(PredicateBase, self).__delattr__(name)

    def sb_check_set(self, gids, lids, obj, uses, allowed, exception, reprtype):
        pass

    def _sb_check_callable(self, gids, lids, obj, uses, allowed, exception, reprtype):
        out = {}
        try:
            out['content'] = ''.join(inspect.getsourcelines(obj.func_code)[0]).strip()
        except IOError as e:
            import pdb; pdb.set_trace()
        out['uses'] = []
        uses.append(out)

        ca = source.CollectArgs(out['content'])
        args = {arg: arg for arg in ca.allargs}
        _lids = obj.__dict__.copy()
        _lids.update(args)

        if type(obj) == type(lambda x: x):
            poseq = out['content'].find('=')
            if poseq > 0:
                self._sb_filluses(obj.func_globals, _lids, \
                    out['content'][poseq+1:].strip(), out['uses'], allowed, exception, reprtype)
            else:
                self._sb_filluses(obj.func_globals, _lids, \
                    out['content'], out['uses'], allowed, exception, reprtype)
        else:
            self._sb_filluses(obj.func_globals, _lids, out['content'], out['uses'], allowed, exception, reprtype)

        if reprtype == SB_SBID_TYPE:
            out['sbid'] = self._sb_getsbid(out['content'], out['uses'])

    def _sb_filluses(self, gids, lids, content, uses, allowed, exception, reprtype):
        '''return standardform of cname
        '''
        # this order should not be changed!!!

        cn = source.CollectName(content)
        for cname in cn.allnames:
            for ns in [ lids, gids ]:
                if cname in ns:
                    if 'callable' in allowed and callable(ns[cname]) and hasattr(ns[cname], 'func_code'):
                        self._sb_check_callable(gids, lids, ns[cname], uses, allowed, exception, reprtype)
                    elif 'set' in allowed and isinstance(ns[cname], Set) and \
                        not isinstance(ns[cname], PrimitiveSet) and getattr(ns[cname], '__exportable__', False):
                        self._sb_check_set(gids, lids, ns[cname], uses, allowed, exception, reprtype)
                    cname = None
            if cname is not None:

                bltin = None
                if isinstance(gids['__builtins__'], dict):
                    bltin = gids['__builtins__']
                elif isinstance(gids['__builtins__'], ModuleType):
                    bltin = dir(gids['__builtins__'])

                if bltin is not None and cname in bltin:
                    pass
                elif cname.startswith('setvar_'):
                    pass
                elif cname.startswith('SB_') and (cname.endswith('_TEST') or cname.endswith('_SET')):
                    import builtinsets as bis
                    if exception and cname not in dir(bis):
                        raise Exception('%s is not in builtinset.'%cname)
                else:
                    if exception:
                        raise Exception('%s is not exportable.'%cname)

    def _sb_getsbid(self, content, uses):
        sha_content = hashlib.sha256(content).hexdigest()
        uses_merged = ''
        for u in uses:
            uses_merged += self._sb_getsbid(u['content'], u['uses'])
        if len(uses_merged) > 0:
            sha_uses = hashlib.sha256(uses_merged).hexdigest()
            return hashlib.sha256(sha_content+sha_uses).hexdigest()
        else:
            return hashlib.sha256(sha_content).hexdigest()

class SetRule(PredicateBase):
    def __init__(self, setvar, settester, setgenerator, wherepart):
        self.setvar = setvar
        self.settester = settester
        self.setgenerator = setgenerator
        self.wherepart = wherepart

    def contains(self, gids, lids, elem, **opts):

        if gids is None or lids is None:
            raise Exception('%s class can not be called without globals and locals.'%self.__class__.__name__)

        if self.setvar.conform(gids, lids, elem, **opts) and \
            self.settester.conform(gids, lids, elem, self.setvar.vartuple, **opts):
            return True
        else:
            return False

    def _sb___len__(self, gids, lids, **opts):
        raise Exception('Predicate set does not support __len__.')

    def _sb___repr__(self, gids, lids, **opts):

        uses = []

        setvar, svuses = self.setvar._sb___repr__(gids, lids, **opts)
        settester, stuses = self.settester._sb___repr__(gids, lids, **opts)
        uses.extend(svuses)
        uses.extend(stuses)

        outstr = ''
        if self.setgenerator and self.wherepart:
            setgenerator, sguses = self.setgenerator._sb___repr__(gids, lids, **opts)
            wherepart, wpuses = self.wherepart._sb___repr__(gids, lids, **opts)
            uses.extend(sguses)
            uses.extend(wpuses)
            outstr = "%s | %s | %s %s"%(setvar, settester, setgenerator, wherepart)
        else:
            outstr = "%s | %s"%(setvar, settester)

        if opts.get('reprtype', SB_DEFAULT_TYPE) == SB_SBID_TYPE:
            outstr = self._sb_getsbid(outstr, uses)

        return outstr, uses

    def _sb___str__(self, gids, lids, **opts):
        if self.setgenerator and self.wherepart:
            return "%s | %s | %s %s"%(self.setvar._sb___str__(gids, lids, **opts), \
                self.settester._sb___str__(gids, lids, **opts), \
                self.setgenerator._sb___str__(gids, lids, **opts), \
                self.wherepart._sb___str__(gids, lids, **opts))
        else:
            return "%s | %s"%(self.setvar._sb___str__(gids, lids, **opts), \
                self.settester._sb___str__(gids, lids, **opts))

    def _sb_choice(self, gids, lids, psetsbid, **opts):

        prevpath = opts['choice']['path'][:]

        cnt = 0
        for _ in range(MAX_COUNT_CHOICE):

            if opts.has_key('timeout') and opts['timeout'] + time.clock() > 0: break

            opts['choice']['path'].append(psetsbid)

            retelem = None

            bltin = None
            if isinstance(gids['__builtins__'], dict):
                bltin = gids['__builtins__']
            elif isinstance(gids['__builtins__'], ModuleType):
                bltin = dir(gids['__builtins__'])

            cn = source.CollectName(self.setgenerator.func)
            subsetvar = cn.removenames(self.setvar.vartuple, gids.keys(), bltin, lids.keys())
            varlist = subsetvar[:]

            # get new sub elems
            retdict = self.wherepart._sb_choice(gids, lids, varlist, **opts)

            if len(varlist) > 0:
                raise Exception('Can not find set variables: %s'%varlist)

            setvars = []
            setelems = []
            setcbs = []

            for setvar, (elem, callback) in retdict.items():
                setvars.extend(setvar.vartuple)
                setelems.extend(elem)
                setcbs.extend(callback)

            lids_copy = {}

            sbmod = sys.modules['setbuilder']
            for k in dir(sbmod):
                if k.startswith('SB_'):
                    lids_copy[k] = getattr(sbmod, k, None)

            if opts.has_key('import_env'):
                for k,v in opts['import_env'].items():
                    lids_copy[k] = v

            for k,v in lids.items():
                lids_copy[k] = v

            for setvar, elem in zip(setvars, setelems):
                lids_copy[setvar] = elem

            if opts['choice'].has_key('indexed_setvars'):
                for sv, idxvar in opts['choice']['indexed_setvars']:
                    lids_copy[sv] = idxvar
            try:
                exec(self.setgenerator._sb___str__(gids, lids, **opts), gids, lids_copy)
            except NameError as e:
                import pdb; pdb.set_trace()
                # if expression contains index self.setvars .... let the setvar as dict variable
                # setcbs[0].im_class is SB_INDEX_SET
            except Exception as e:
                pass
            finally:
                #cnt += 1
                #if cnt < 100:
                #    continue
                #raise
                pass

            try:
                # TODO: may split settest to multiple CNF tests
                teststr = self.settester._sb___str__(gids, lids, **opts)
                exec('_sb_result = %s'%teststr, gids, lids_copy)
                sb_result = lids_copy['_sb_result']
                if sb_result == True:
                    if len(self.setvar.vartuple) > 1:
                        retelem = tuple([ lids_copy[vt] for vt in self.setvar.vartuple ])
                    else:
                        retelem =lids_copy[self.setvar.vartuple[0]]
                    if psetsbid ==  opts['choice']['path'][0]:
                        map(operator.methodcaller('__call__', True), setcbs)
                else:
                    map(operator.methodcaller('__call__', False), setcbs)

            except Exception as e:
                #import pdb; pdb.set_trace()
                pass

            if retelem is not None:
                break

            opts['choice']['path'] = []

        opts['choice']['path'] = prevpath

        # TODO: split tester to many CNF, and each term is evaluated seperately, and 
        # find what setvar are involved and feedback using approproate callbacks
        # create a new callback for Set object that only checks dupulicates
        #import pdb; pdb.set_trace()
        #return retelem, lambda result: map(operator.methodcaller('__call__', result), setcbs)

        callback = functools.partial(self.callback, setcbs=setcbs)
        return retelem, callback

    def callback(self, result, setcbs=None):
        for setcb in setcbs:
            if isinstance(setcb, list):
                for cb in setcb:
                    cb(result)
            else:
                setcb(result)

    def tree(self, depth=0):
        l = []

        l.append('    '*depth + '%s: '%self.__class__.__name__ + self._sb___str__(gids, lids, **opts))

        for elem in [ self.setvar, self.settester, self.setgenerator, self.wherepart ]:
            if hasattr(elem, 'tree'):
                l.append('    '*(depth+1) + elem.tree(depth=depth+1))
            else:
                l.append('    '*(depth+1) + elem._sb___str__(gids, lids, **opts))

        return '\n'.join(l)

class SetVar(PredicateBase):
    def __init__(self, vartuple):
        self.vartuple = tuple(vartuple)
        self.index = None

    def _sb___repr__(self, gids, lids, **opts):
        content = None
        if len(self.vartuple) > 1:
            if self.index is not None:
                content = '( %s )'%', '.join([ 'setvar_%s%d_%d'%\
                    (self.varmapname,self.index,i) for i in range(len(self.vartuple))])
            else:
                content = '( %s )'%', '.join([ 'setvar_%s%d'%(self.varmapname,i) for i in range(len(self.vartuple))])
        else:
            if self.index is not None:
                content = 'setvar_%s%d'%(self.varmapname, self.index)
            else:
                content = 'setvar_%s'%self.varmapname

        return content, []

    def _sb___str__(self, gids, lids, **opts):
        if len(self.vartuple) > 1:
            return '( %s )'%', '.join([ str(v) for v in self.vartuple])
        else:
            return str(self.vartuple[0])

    def conform(self, gids, lids, elem, **opts):
        if elem.__class__ in [ list, tuple, set ]:
            if len(self.vartuple) == len(elem):
                return True
        elif len(self.vartuple) == 1:
            return True
        return False

class RuleSetVar(SetVar):
    varmapname = 'x'

class WhereSetVar(SetVar):
    varmapname = 'y'

class SetTester(PredicateBase):
    def __init__(self, test, setvar):
        self.test = test
        self.setvar = setvar

    def _sb___str__(self, gids, lids, **opts):
        return str(self.test)

    def _sb___repr__(self, gids, lids, **opts):

        namemap = {}
        if len(self.setvar.vartuple) > 1:
            for i, var in enumerate(self.setvar.vartuple):
                namemap[var] = 'setvar_x%d'%i
        else:
            namemap[self.setvar.vartuple[0]] = 'setvar_x'

        rn = source.RewriteName(self.test, namemap)

        content = astunparse.unparse(rn.tree).strip()
        if len(content)>1 and content[0] == '(' and content[-1] == ')':
            content = content[1:-1].strip()

        # update content with stdform of function, set names

        # create another namemap and send it to self._sb_filluses

        uses = []
        if opts.get('complete', False):
            if opts.has_key('import_env'):
                self._sb_filluses(opts['import_env'], lids, content, uses, \
                    ('callable', 'set'), opts.get('raise', True), opts.get('reprtype', SB_DEFAULT_TYPE))
            else:
                self._sb_filluses(gids, lids, content, uses, \
                    ('callable', 'set'), opts.get('raise', True), opts.get('reprtype', SB_DEFAULT_TYPE))

        if opts.get('reprtype', SB_DEFAULT_TYPE) == SB_SBID_TYPE:
            content = self._sb_getsbid(content, uses)

        return content, uses

    def conform(self, gids, lids, elem, varname, **opts):
        '''Check if elem satisfies set test

            NOTE: The last statment of set rule should only use set variable name in SetVar
        '''

        try:
            for _ in elem:
                break
        except:
            elem = ( elem, )

        if len(elem) != len(varname):
            return False

        lids_copy = dict(lids)

        sbmod = sys.modules['setbuilder']
        for k in dir(sbmod):
            if k.startswith('SB_'):
                lids_copy[k] = getattr(sbmod, k, None)

        for v, e in zip(varname, elem):
            lids_copy[v] = e

        result = False
        try:
            exec('_sb_result = %s'%str(self.test), gids, lids_copy)
            if lids_copy['_sb_result']:
                result = True
        except:
            pass

        return result

class SetGenerator(PredicateBase):
    def __init__(self, func, setrulevar, setwherevarlist):
        self.func = func
        self.setrulevar = setrulevar
        self.setwherevarlist = setwherevarlist

    def _sb___str__(self, gids, lids, **opts):
        return str(self.func)

    def _sb___repr__(self, gids, lids, **opts):

        namemap = {}

        if len(self.setrulevar.vartuple) > 1:
            for i, var in enumerate(self.setrulevar.vartuple):
                namemap[var] = 'setvar_x%d'%i
        else:
            namemap[self.setrulevar.vartuple[0]] = 'setvar_x'

        for swv in self.setwherevarlist:
            if len(swv.vartuple) > 1:
                for i, var in enumerate(swv.vartuple):
                    namemap[var] = 'setvar_y%d_%d'%(swv.index, i)
            else:
                namemap[swv.vartuple[0]] = 'setvar_y0'

        rn = source.RewriteName(self.func, namemap)

        content = astunparse.unparse(rn.tree).strip()

        uses = []
        if opts.get('complete', False):
            if opts.has_key('import_env'):
                self._sb_filluses(opts['import_env'], lids, content, uses, \
                    ('callable', 'set'), opts.get('raise', True), opts.get('reprtype', SB_DEFAULT_TYPE))
            else:
                self._sb_filluses(gids, lids, content, uses, \
                    ('callable', 'set'), opts.get('raise', True), opts.get('reprtype', SB_DEFAULT_TYPE))

        if opts.get('reprtype', SB_DEFAULT_TYPE) == SB_SBID_TYPE:
            content = self._sb_getsbid(content, uses)

        return content, uses

class WherePart(PredicateBase):
    def __init__(self, wherebodylist):
        self.wherebodylist = wherebodylist

    def _sb___str__(self, gids, lids, **opts):
        return 'where %s'%' and '.join([ wherebody._sb___str__(gids, lids, **opts) for wherebody in self.wherebodylist])

    def _sb___repr__(self, gids, lids, **opts):
        wbodies = []
        uses = []
        for wherebody in self.wherebodylist:
            wbody, buse = wherebody._sb___repr__(gids, lids, **opts)
            wbodies.append(wbody)
            uses.extend(buse)
        outstr = 'where %s'%' and '.join(wbodies)
        return outstr, uses

    def _sb_choice(self, gids, lids, setvar, **opts):

        if gids is None or lids is None:
            raise Exception('%s class can not be called without globals and locals.'%self.__class__.__name__)


        prevpath = opts['choice']['path'][:]

        retdict = {}
        for i, body in enumerate(self.wherebodylist):
            opts['choice']['path'].append('%d'%i)
            body._sb_choice(gids, lids, setvar, retdict, **opts)
            opts['choice']['path'] = prevpath[:]

        return retdict


    def tree(self, depth=0):
        l = []

        l.append('    '*depth + '%s: '%self.__class__.__name__ + self._sb___str__(gids, lids, **opts))

        for wherebody in self.wherebodylist:
            if hasattr(wherebody, 'tree'):
                l.append('    '*(depth+1) + wherebody.tree(depth=depth+1))
            else:
                l.append('    '*(depth+1) + wherebody._sb___str__(gids, lids, **opts))

        return 'where %s'%'\n'.join(l)

class WhereBody(PredicateBase):
    def __init__(self, setvar, setexpr, **opts):
        self.setvar = setvar
        self.setexpr = setexpr
        if isinstance(setexpr, Set):
            self.setobj = setexpr
        elif isinstance(setexpr, str):
            self.setobj = _get_set(setexpr, gids=opts.get('gids', None), lids=opts.get('lids', None))
        else:
            raise Exception('Not supported type: %s'%type(setexpr))

    def _sb___str__(self, gids, lids, **opts):
        if isinstance(self.setexpr, Set):
            return "%s in %s"%(self.setvar._sb___str__(gids, lids, **opts), \
                self.setexpr._sb___str__(gids, lids, **opts))
        else:
            return "%s in %s"%(self.setvar._sb___str__(gids, lids, **opts), str(self.setexpr))

    def _sb___repr__(self, gids, lids, **opts):

        setvar = None
        if len(self.setvar.vartuple) > 1:
            rnlist = []
            for i, var in enumerate(self.setvar.vartuple):
                rnlist.append('setvar_y%d_%d'%(self.setvar.index, i))
            setvar = '( %s )'%', '.join(rnlist)
        else:
            setvar = 'setvar_y0'

        setexpr = self.setexpr
        uses = []
        if isinstance(self.setobj, Set):
            if opts.get('reprtype', SB_DEFAULT_TYPE) == SB_EXPORT_TYPE:
                import builtinsets as bis
                if not self.setexpr in dir(bis):
                    setout = self.setobj._sb___repr__(gids, lids, **opts)
                    if opts.get('complete', False):
                        setout['content'] = '%s = setbuilder.build("%s")'%(self.setexpr, setout['content'])
                        uses.append(setout)
            elif opts.get('reprtype', SB_DEFAULT_TYPE) == SB_SBID_TYPE:
                setout = self.setobj._sb___repr__(gids, lids, **opts)
                setexpr = setout['content']
                uses.extend(setout.get('uses', []))
        else:
            if opts.get('raise', False):
                raise Exception('Set name, %s, does not exist in setbuilder module.'%self.setexpr)

        content = "%s in %s"%(setvar, setexpr)

        return content, uses

    def _sb_choice(self, gids, lids, setvar, retdict, **opts):

        if gids is None or lids is None:
            raise Exception('%s class can not be called without globals and locals.'%self.__class__.__name__)

        elem = None
        callback = None
        if any( (varname in self.setvar.vartuple) for varname in setvar ):

            for vn in self.setvar.vartuple:
                if vn in setvar:
                    setvar.remove(vn)

            if isinstance(self.setobj, Set):
                elem, callback = self.setobj._sb_choice(gids, lids, **opts)
                if len(self.setvar.vartuple) == 1:
                    elem = ( elem, )
                    callback = ( callback, )
                else:
                    callback = tuple( [ callback ]*len(elem) )
            else:
                raise Exception('Wrong type in wherebody: %s'%str(self.setexpr))

        if elem is not None:
            retdict[self.setvar] = ( elem, callback )

class WhereBracketBody(PredicateBase):
    def __init__(self, setvar, setexpr, repeat, **opts):
        self.setvar = setvar
        self.repeat = repeat
        self.setexpr = setexpr
        if isinstance(setexpr, Set):
            self.setobj = setexpr
        elif isinstance(setexpr, str):
            self.setobj = _get_set(setexpr, gids=opts.get('gids', None), lids=opts.get('lids', None))
        else:
            raise Exception('Not supported type: %s'%type(setexpr))

    def _sb___str__(self, gids, lids, **opts):
        if isinstance(self.setexpr, Set):
            return "%s in [ %s ] * %s"%(self.setvar._sb___str__(gids, lids, **opts), \
                self.setexpr._sb___str__(gids, lids, **opts), str(self.repeat))
        else:
            return "%s in [ %s ] * %s"%(self.setvar._sb___str__(gids, lids, **opts), \
                str(self.setexpr), str(self.repeat))

    def _sb___repr__(self, gids, lids, **opts):

        setvar = None
        if len(self.setvar.vartuple) > 1:
            rnlist = []
            for i, var in enumerate(self.setvar.vartuple):
                rnlist.append('setvar_y%d_%d'%(self.setvar.index, i))
            setvar = '( %s )'%', '.join(rnlist)
        else:
            setvar = 'setvar_y0'

        sbset = None
        if isinstance(self.setobj, Set):
            sbset = self.setobj
        else:
            if opts.get('raise', False):
                raise Exception('Wrong type in wherebody: %s'%str(self.setexpr))

        setexpr = self.setexpr
        uses = []
        if isinstance(sbset, Set):
            if opts.get('reprtype', SB_DEFAULT_TYPE) == SB_EXPORT_TYPE:
                import builtinsets as bis
                if not self.setexpr in dir(bis):
                    setout = sbset._sb___repr__(gids, lids, **opts)
                    if opts.get('complete', False):
                        setout['content'] = '%s = setbuilder.build("%s")'%(self.setexpr, setout['content'])
                        uses.append(setout)
            elif opts.get('reprtype', SB_DEFAULT_TYPE) == SB_SBID_TYPE:
                setout = sbset._sb___repr__(gids, lids, **opts)
                setexpr = setout['content']
                uses.extend(setout.get('uses', []))
        else:
            if opts.get('raise', False):
                raise Exception('Set name, %s, does not exist in setbuilder module.'%self.setexpr)

        content = "%s in [ %s ] * %s"%(setvar, setexpr, str(self.repeat))

        return content, uses

    def _sb_choice(self, gids, lids, setvar, retdict, **opts):

        if gids is None or lids is None:
            raise Exception('%s class can not be called without globals and locals.'%self.__class__.__name__)

        elem = [[]*len(self.setvar.vartuple)]
        callback = [[]*len(self.setvar.vartuple)]
        if any( (varname in self.setvar.vartuple) for varname in setvar ):

            for vn in self.setvar.vartuple:
                if vn in setvar:
                    setvar.remove(vn)

            cnt = 0
            if isinstance(self.setobj, Set):
                while len(elem[0]) < self.repeat and cnt < self.repeat * MAX_COUNT_CHOICE:
                    e, c = self.setobj._sb_choice(gids, lids, **opts)
                    if e is not None:
                        if len(self.setvar.vartuple) == 1:
                            elem[0].append(e)
                            callback[0].append(c)
                        else:
                            for i in range(len(e)):
                                elem[i].append(e[i])
                                callback[i].append(c)
                    cnt += 1
            else:
                raise Exception('Wrong type in wherebody: %s'%str(self.setexpr))


        if elem is not None:
            retdict[self.setvar] = ( elem, callback )

###########################################################################
# Mixed set
###########################################################################

class MixedSet(Set):
    '''EnumSet + PredicateSet

    '''

    def __init__(self, *args, **opts):

        self.__indexed__ =  opts.pop('indexed', False)
        if self.__indexed__:
            self.elems = OrderedDict()
        else:
            self.elems = dict()
        self.enums = dict()
        self.sets = dict()
        self.rules = dict()

        for arg in args:
            if isinstance(arg, str):
                arg = _get_set(arg, opts.get('gids', None), opts.get('lids', None))

            if isinstance(arg, EnumSet):
                for elem, attr in arg.elems.items():
                    self.elems[elem] = attr
                    self.enums[elem] = attr
            elif isinstance(arg, PredicateSet):
                self.elems[arg.setrule] = None
                self.rules[arg.setrule] = None
            elif isinstance(arg, PrimitiveSet):
                self.elems[arg] = None
                self.sets[arg] = None
            elif isinstance(arg, CompoundSet):
                self.elems[arg] = None
                self.sets[arg] = None
            elif isinstance(arg, MixedSet):
                self.elems[arg] = None
                self.sets[arg] = None
            elif isinstance(arg, SetRule):
                self.elems[arg] = None
                self.rules[arg] = None
            else:
                try:
                    for elem, attr in arg.items():
                        self.elems[elem] = attr
                        if isinstance(elem, SetRule):
                            self.rules[elem] = None
                        else:
                            self.enums[elem] = None
                except:
                    for elem in arg:
                        self.elems[elem] = None
                        if isinstance(elem, SetRule):
                            self.rules[elem] = None
                        else:
                            self.enums[elem] = None

        super(MixedSet, self).__init__(**opts)

        self.__frozen__ = opts.pop('frozen', True)

    def _sb___str__(self, gids, lids, **opts):
        l = []
        for e in self.elems:
            if isinstance(e, Set):
                l.append(e._sb___str__(gids, lids, **opts))
            elif isinstance(e, SetRule):
                l.append(e._sb___str__(gids, lids, **opts))
            else:
                l.append(str(e))
        return '{ %s }'%', '.join(l)

    def _sb_choice(self, gids, lids, **opts):

        callback = lambda result: True

        if len(self.elems) == 0:
            return None, callback

        elem = random.choice(self.elems.keys())

        if elem in self.enums:
            pass
        elif elem in self.rules:
            elem, callback = elem._sb_choice(gids, lids, self._sb_sbid(gids, lids, **opts), **opts)
        elif elem in self.sets:
            elem, callback = elem._sb_choice(gids, lids, **opts)
        else:
            raise Exception('Unknown elem')

        return elem, callback

    def _sb___contains__(self, gids, lids, elem, **opts):
        for _elem in self.elems:
            if isinstance(_elem, Set):
                if _elem._sb___contains__(gids, lids, elem, **opts):
                    return True
            elif isinstance(_elem, SetRule):
                if _elem.contains(gids, lids, elem, **opts):
                    return True
            elif elem == _elem:
                return True
        return False
