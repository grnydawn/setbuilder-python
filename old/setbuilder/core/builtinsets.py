# -*- coding: UTF-8 -*-

import sys
import random
from basesets import PrimitiveSet
from functions import build
from config import SB_MAXINT_VALUE, SB_MININT_VALUE, SB_MAXFLOAT_VALUE, SB_MINFLOAT_VALUE

# TODO: add index specifying grammar [ SET ] * (1,10,2)
# TODO: represents primitive value as a binary form
# TODO: generates multiple tests from even a single boolean form

###########################################################################
# Primitive Sets
###########################################################################

# Test Functions
SB_BOOLEAN_TEST        = lambda x: type(x) == type(True)
SB_INT_TEST            = lambda x: type(x) == type(1)
SB_FLOAT_TEST          = lambda x: type(x) == type(1.0)

# Set construction
class SB_BOOLEAN_SET(PrimitiveSet):

    def __init__(self, **opts):
        self.__indexed__ = True
        super(SB_BOOLEAN_SET, self).__init__(**opts)

    def _sb___contains__(self, gids, lids, elem, **opts):
        return SB_BOOLEAN_TEST(elem)

    def _sb_choice(self, gids, lids, **opts):
        return random.choice([True, False]), lambda result: True

class SB_FLOATNUM_SET(PrimitiveSet):

    _MAXVAL = SB_MAXFLOAT_VALUE
    _MINVAL = SB_MINFLOAT_VALUE
    _BITLEN = 64

    def __init__(self, **opts):
        self.__indexed__ = False
        super(SB_FLOATNUM_SET, self).__init__(**opts)

    def _sb___contains__(self, gids, lids, elem, **opts):
        return SB_FLOAT_TEST(elem)

    def _sb_choice(self, gids, lids, **opts):
        opts['choice']['path'].append(self._sb_sbid(gids, lids, **opts))
        return self._sb_genelems(gids, lids, opts['choice']['path'], \
            opts.get('cache', True))

class SB_INTNUM_SET(PrimitiveSet):

    _MAXVAL = SB_MAXINT_VALUE
    _MINVAL = SB_MININT_VALUE
    _BITLEN = 32

    def __init__(self, **opts):
        self.__indexed__ = True
        super(SB_INTNUM_SET, self).__init__(**opts)

    def _sb___contains__(self, gids, lids, elem, **opts):
        return SB_INT_TEST(elem)

    def _sb_choice(self, gids, lids, **opts):

        opts['choice']['path'].append(self._sb_sbid(gids, lids, **opts))

        return self._sb_genelems(gids, lids, opts['choice']['path'], \
            opts.get('cache', True))

class SB_CHAR_SET(PrimitiveSet):

    _MAXVAL = 255
    _MINVAL = 0
    _BITLEN = 8

    def __init__(self, **opts):
        self.__indexed__ = True
        super(SB_CHAR_SET, self).__init__(**opts)

    def _sb___contains__(self, gids, lids, elem, **opts):
        return ord(elem) >= self._MINVAL and ord(elem) <= self._MAXVAL

    def _sb_choice(self, gids, lids, **opts):

        opts['choice']['path'].append(self._sb_sbid(gids, lids, **opts))

        elem, callback = self._sb_genelems(gids, lids, opts['choice']['path'], \
            opts.get('cache', True))
        if elem is None:
            return elem, callback
        else:
            return chr(elem), callback

class SB_UNICODE_SET(PrimitiveSet):

    _MAXVAL = 65535
    _MINVAL = 0
    _BITLEN = 16

    def __init__(self, **opts):
        self.__indexed__ = True
        super(SB_UNICODE_SET, self).__init__(**opts)

    def _sb___contains__(self, gids, lids, elem, **opts):
        return ord(elem) >= self._MINVAL and ord(elem) <= self._MAXVAL

    def _sb_choice(self, gids, lids, **opts):

        opts['choice']['path'].append(self._sb_sbid(gids, lids, **opts))

        elem, callback = self._sb_genelems(gids, lids, opts['choice']['path'], \
            opts.get('cache', True))
        if elem is None:
            return elem, callback
        else:
            return unichr(elem), callback

###########################################################################
# Number Sets
###########################################################################

# Test Functions
SB_NATURAL_TEST         = lambda x: SB_INT_TEST(x) and x > 0
SB_EVENNUM_TEST         = lambda x: SB_NATURAL_TEST(x) and x%2 == 0
SB_ODDNUM_TEST          = lambda x: SB_NATURAL_TEST(x) and x%2 == 1
SB_REALNUM_TEST         = lambda x: SB_INT_TEST(x) or SB_FLOAT_TEST(x)
SB_COMPLEXNUM_TEST      = lambda x: type(x) == type(1j)
SB_NUM_TEST             = lambda x: SB_REALNUM_TEST(x) or SB_COMPLEXNUM_TEST(x)


# Set construction
SB_NATURALNUM_SET       = build('{ x | SB_NATURAL_TEST(x) | x = n where n in SB_INTNUM_SET }')
SB_ODDNUM_SET           = build('{ x | SB_ODDNUM_TEST(x) | x = n where n in SB_INTNUM_SET }')
SB_EVENUM_SET           = build('{ x | SB_EVENNUM_TEST(x)| x = n where n in SB_INTNUM_SET }')
SB_REALNUM_SET          = build('{ x | SB_REALNUM_TEST(x) | x = n where n in SB_INTNUM_SET | SB_FLOATNUM_SET }')
SB_COMPLEXNUM_SET       = build('{ x | SB_COMPLEXNUM_TEST(x) | x = complex(n[0], n[1]) \
                            where n in [ SB_FLOATNUM_SET | SB_FLOATNUM_SET ] * 2 }')
SB_NUM_SET              = build('{ x | SB_NUM_TEST(x) | x = n where n in SB_REALNUM_SET | SB_COMPLEXNUM_SET }')


###########################################################################
# Character Sets
###########################################################################

# Test Functions
SB_ASCII_TEST           = lambda x: x.encode('ascii')
SB_LATIN1_TEST          = lambda x: x.encode('latin1')
SB_ASCIIPRINTABLE_TEST  = lambda x: SB_ASCII_TEST(x) and ord(x) >= 32
SB_ASCIICONTROL_TEST    = lambda x: SB_ASCII_TEST(x) and ord(x) < 32
SB_ASCIIEXTENDED_TEST   = lambda x: SB_LATIN1_TEST() and ord(x) >= 127

# Set construction
SB_ASCII_SET            = build('{ x | SB_ASCII_TEST(x) | x = c where c in SB_CHAR_SET }')
SB_LATIN1_SET           = build('{ x | SB_LATIN1_TEST(x) | x = c where c in SB_CHAR_SET }')
SB_ASCIIPRINTABLE_SET   = build('{ x | SB_ASCIIPRINTABLE_TEST(x) | x = c where c in SB_CHAR_SET }')
SB_ASCIIEXTENDED_SET    = build('{ x | SB_ASCIIEXTENDED_TEST(x) | x = c where c in SB_CHAR_SET }')
SB_ASCIICONTROL_SET     = build('{ x | SB_ASCIICONTROL_TEST(x) | x = c where c in SB_CHAR_SET }')
