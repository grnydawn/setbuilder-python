'''Source Translation
'''

import ast

class CollectName(ast.NodeVisitor):

    def __init__(self, src):

        self.namenodes = []
        self.allnames = []

        if isinstance(src, ast.Module):
            self.visit(src)
        else:
            self.visit(ast.parse(src))

    def visit_Name(self, node):
        self.namenodes.append(node)
        if node.id not in self.allnames:
            self.allnames.append(node.id)

    def removenames(self, *args):

        names = []
        for name in self.allnames:
            if any( name in arg for arg in args):
                pass
            else:
                names.append(name)
        return names

class RewriteName(ast.NodeTransformer):

    def __init__(self, src, namemap):

        self.tree = None
        self.namemap = namemap

        if isinstance(src, ast.Module):
            self.tree = src
        else:
            self.tree = ast.parse(src)
        self.visit(self.tree)

    def visit_Name(self, node):
        if node.id in self.namemap:
            node.id = self.namemap[node.id]
        return node

class CollectArgs(ast.NodeVisitor):

    def __init__(self, src):

        self.argnodes = []
        self.allargs = []

        if isinstance(src, ast.Module):
            self.visit(src)
        else:
            self.visit(ast.parse(src))

    def visit_arguments(self, node):
        if node.args is not None:
            for arg in node.args:
                if arg.id not in self.allargs:
                    self.allargs.append(arg.id)
                    self.argnodes.append(arg)

        if node.vararg is not None:
            import pdb; pdb.set_trace()

        if node.kwarg is not None:
            import pdb; pdb.set_trace()
