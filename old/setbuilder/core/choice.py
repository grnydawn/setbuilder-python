'''Choice 
'''

import math
import random

SB_END_CHOICE = 2
SB_LARGER_CHOICE = 1
SB_ACCEPTED_CHOICE = 0
SB_SMALLER_CHOICE = -1
SB_DUPLICATE_CHOICE = -2

def select(algos):
    weights = []
    for algo in algos:
        weight = int(math.log10(max(algo.hits, 10)))
        adj = int(math.log10(max(algo.hits/max(algo.misses, 1), 10)))
        weights.append((weight, adj))
    draw = []
    for i, (w,a) in enumerate(weights):
        draw.extend([i]*w*a)
    chosen = random.choice(draw)
    return algos[chosen]

class Choice(object):
    def __init__(self, bitlen, minval, maxval):
        self.bitlen = bitlen
        self.minval = minval
        self.maxval = maxval
        self.misses = 0
        self.hits = 0
        self.choicedb = {}

    def generate(self, cache):
        raise Exception('Subclass has to implement generate method.')

    def update(self, result):
        raise Exception('Subclass has to implement update method.')

    def dbexport(self):
        return []
