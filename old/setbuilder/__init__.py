"""setbuilder-python module

"""


# need sanity checks


# imports
import os
from .core import *
from .choice import *

# initialize setbuilder
for f in os.listdir('%s/.setbuilder/cache'%os.path.expanduser('~')):
    if f.endswith(".py"):
        pathcode, _ = f.split('.')
        pathcode = pathcode[1:]
        sbcachefile = '%s/.setbuilder/cache/C%s.py'%(os.path.expanduser('~'), pathcode)
        with open(sbcachefile, 'r') as f:
            exec(f.read())
            PrimitiveSet._cmap[pathcode] = cached

# finalize setbuilder
def finalize():
    for pathcode, algos in PrimitiveSet._amap.items():
        srclines = []
        for i, algo in enumerate(algos):
            clsname =  algo.__class__.__name__
            srcline = 'from setbuilder import %s\nalgorithm_%d = %s(%s, %s, %s)'
            srclines.append(srcline%(clsname, i, clsname, str(algo.bitlen), str(algo.minval), str(algo.maxval)))
            srclines.append('algorithm_%d.hits = %d'%(i, algo.hits))
            srclines.append('algorithm_%d.misses = %d'%(i, algo.misses))
            for name, value in algo.dbexport():
                srclines.append('algorithm_%d.choicedb["%s"] = %s\n'%(i, name, value))
            srclines.append('\n')

        sbalgofile = '%s/.setbuilder/choice/A%s.py'%(os.path.expanduser('~'), pathcode)
        with open(sbalgofile, 'w') as f:
            f.write('\n'.join(srclines))

    for pathcode, cache in PrimitiveSet._cmap.items():
        sbcachefile = '%s/.setbuilder/cache/C%s.py'%(os.path.expanduser('~'), pathcode)
        with open(sbcachefile, 'w') as f:
            f.write('cached = %s'%str(cache))

import atexit
atexit.register(finalize)
