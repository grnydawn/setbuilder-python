'''Sliding Distribution Algorithm

'''

from setbuilder import Choice
import random
import math

class SlidingDistribution(Choice):
    def __init__(self, *args, **opts):
        super(SlidingDistribution, self).__init__(*args, **opts)

        if isinstance(self.minval, int):
            self.choicedb['normparams'] = [ 0.0, 10.0 ]
        elif isinstance(self.minval, float):
            self.choicedb['normparams'] = [ 0.0, 10.0 ]
            self.choicedb['explimits'] = ( math.log10(abs(self.minval)), abs(math.log10(self.maxval)) )
        else:
            raise Exception('Not supported type: %s'%str(type(self.minval)))

    def generate(self):

        if isinstance(self.minval, int):
            return self.intsamplefunc(), self.intupdate
        elif isinstance(self.minval, float):
            return self.floatsamplefunc(), self.floatupdate
        else:
            raise Exception('Not supported type: %s'%str(type(self.minval)))

    def intsamplefunc(self):
        val = None
        mu, sigma = self.choicedb['normparams']
        x = len(self.choicedb['cache'])
        adj = (x + 10) / ( x + 1)
        if mu is not None and sigma is not None:
            while val is None:
                val = int(random.normalvariate(mu, sigma * adj))
                if val > self.maxval or val < self.minval:
                    val = None
        return (val, val)

    def floatsamplefunc(self):

        val = None
        expmu, expsigma = self.choicedb['normparams']
        x = len(self.choicedb['cache'])
        adj = (x + 10) / ( x + 1)

        if expmu is not None and expsigma is not None:
            cnt = 0
            while val is None and cnt < 100:
                exp = random.normalvariate(expmu, expsigma * adj)
                sig = random.uniform(-1.0, 1.0)
                cnt += 1
                if exp < self.choicedb['explimits'][0] or exp > self.choicedb['explimits'][1]:
                    continue
                val = sig * 10**exp
                if val > self.maxval or val < self.minval:
                    val = None
        return (val, val)

    def intfit(self):
        cache = self.choicedb['cache']
        mu = sum(cache) / float(len(cache))
        try:
            sigma = math.sqrt(sum(map(lambda x: (x-mu)**2, cache))/float(len(cache)))
            if sigma == 0:
                sigma = mu*10
        except:
            sigma = self.choicedb['normparams'][1]
        return [ mu, sigma ]

    def floatfit(self):

        exps = map(lambda x: math.log10(abs(x)), self.choicedb['cache'])
        mu = sum(exps) / float(len(exps))
        try:
            sigma = math.sqrt(sum(map(lambda x: (x-mu)**2, exps))/float(len(exps)))
            if sigma == 0:
                sigma = mu*10
        except:
            sigma = self.choicedb['normparams'][1]
        return [ mu, sigma ]

    def intupdate(self, result, elem, retval):
        if isinstance(result, bool):
            if result:
                self.choicedb['normparams'] = self.intfit()
            else:
                mu = self.choicedb['normparams'][0]
                if mu == 0.0:
                    self.choicedb['normparams'][1] = max(random.normalvariate(mu, 1.0), 0.5)
                else:
                    self.choicedb['normparams'][1] = max(random.normalvariate(mu*2, mu*10), mu*0.5)
        elif isinstance(result, int):
            pass
        elif isinstance(result, float):
            pass
        else:
            raise Exception('Not supported result type: %s'%str(result))

    def floatupdate(self, result, elem, retval):
        if isinstance(result, bool):
            if result:
                self.choicedb['normparams'] = self.floatfit()
            else:
                mu = self.choicedb['normparams'][0]
                if mu == 0.0:
                    self.choicedb['normparams'][1] = max(random.normalvariate(mu, 1.0), 0.5)
                else:
                    self.choicedb['normparams'][1] = max(random.normalvariate(mu*2, mu*10), mu*0.5)
        elif isinstance(result, int):
            pass
        elif isinstance(result, float):
            pass
        else:
            raise Exception('Not supported result type: %s'%str(result))

    def dbexport(self):
        return [ ( 'normparams', str(self.choicedb['normparams']) ) ]
