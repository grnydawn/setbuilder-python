'''Random choice

'''

from setbuilder import Choice, SB_ACCEPTED_CHOICE
import random

class RandomChoice(Choice):
    def generate(self):

        if isinstance(self.minval, int):
            randfunc = random.randint
        elif isinstance(self.minval, float):
            randfunc = random.uniform
        else:
            raise Exception('Not supported type: %s'%str(type(self.minval)))

        elem = None
        while elem is None:
            elem = randfunc(self.minval, self.maxval)
        return (elem, elem), self.update

    def update(self, result, elem, retval):
        pass
