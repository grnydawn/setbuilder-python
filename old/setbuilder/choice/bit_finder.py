'''Sliding Distribution Algorithm

'''

from setbuilder import Choice
import random
import math

# integer helper functions
binstr = lambda n,l: format(2**l + n, 'b')[-l:]
binary = lambda n,l: map(int, list(binstr(n,l)))

class BitFinder(Choice):
    def __init__(self, *args, **opts):
        super(BitFinder, self).__init__(*args, **opts)

        if isinstance(self.minval, int):
            self.choicedb['bithitmiss'] = self.init_bithitmiss() # hit, miss of a bit
            self.bitseq = range(len(self.choicedb['bithitmiss']))
            self.choicedb['lenbits'] = self.init_lenbits() # hit, miss of number of bits
            self.nbitseq = range(len(self.choicedb['lenbits'])+1)
            self.choicedb['signbit'] = [ [1, 1], [1, 1] ] # hit, miss of sign bit 0, and 1
            self.sbitseq = [ 0, 1 ]
        elif isinstance(self.minval, float):
            pass
        else:
            raise Exception('Not supported type: %s'%str(type(self.minval)))

    def init_lenbits(self):
        N = 7
        lenbits = []
        lenbits.append([ 2**N, 2**N ])
        size = self.bitlen - 1
        for i in range(1, size):
            val = lenbits[i-1][0]
            if i < N:
                lenbits.append([val >> 1, val >> 1])
            #elif i >= size - int(N/2):
            #    lenbits.append([val << 1, val << 1])
            else:
                lenbits.append([1, 1])

        return lenbits

    def init_bithitmiss(self):
        bithitmiss = []
        for _ in range(self.bitlen - 1):
            bithitmiss.append([1, 1])
        return bithitmiss

    def choice_dist(self, seq, pdf):
        x = random.uniform(0, 1)
        CDF = 0.0
        for item, prob in zip(seq, pdf):
            CDF += prob
            if x < CDF: break
        return item

    def hm2pdf(self, hm):
        pdf = [ 0.0 ] * len(hm)
        for i, (h, m) in enumerate(hm):
            w = math.log10(max(h, 10))
            a = math.log10(max(h/max(m, 1), 10))
            pdf[i] = w * a
        s = sum(pdf)
        pdf = map(lambda p: p / s, pdf)
        return pdf

    def bits2int(self, sbit, bits):
        out = 0
        for bit in bits:
            out = (out << 1) | bit
        if sbit:
            return -out
        else:
            return out

    def generate(self):
        if isinstance(self.minval, int):
            return self.intsamplefunc(), self.intupdate
        elif isinstance(self.minval, float):
            elem = None
            while elem is None:
                elem = random.uniform(self.minval, self.maxval)
            return (elem, elem), self.floatupdate
        else:
            raise Exception('Not supported type: %s'%str(type(self.minval)))

    def intsamplefunc(self):
        val = None
        cnt = 0
        while (val is None and cnt < 100):
            # select bits to be 1
            sbitpdf = self.hm2pdf(self.choicedb['signbit'])
            signbit = self.choice_dist(self.sbitseq, sbitpdf)

            # decide how many bits will be used with sign bit selection
            nbitpdf = self.hm2pdf(self.choicedb['lenbits'])
            nbits = self.choice_dist( self.nbitseq, nbitpdf )

            # select N bits 
            bits = [ 0 ] * len(self.bitseq)
            bitpdf = self.hm2pdf(self.choicedb['lenbits'])
            for _ in range(nbits):
                cnt = 0
                while(cnt < 100):
                    bit = self.choice_dist( self.bitseq, bitpdf )
                    if bits[bit] == 0:
                        bits[bit] = 1
                        break
                    cnt += 1

            # convert bits to int value
            val = self.bits2int(signbit, bits)

            if val > self.maxval or val < self.minval:
                val = None

            cnt += 1

        return val, (signbit, bits)

    def intupdate(self, result, elem, retval):
        if isinstance(result, bool):
            if result:
                self.choicedb['signbit'][retval[0]][0] += 1
                for i, bit in enumerate(retval[1]):
                    if bit:
                        self.choicedb['bithitmiss'][i][0] += 1
                self.choicedb['lenbits'][sum(retval[1])][0] += 1
            else:
                self.choicedb['signbit'][retval[0]][1] += 1
                for i, bit in enumerate(retval[1]):
                    if bit:
                        self.choicedb['bithitmiss'][i][1] += 1
                self.choicedb['lenbits'][sum(retval[1])][1] += 1
        elif isinstance(result, int):
            pass
        elif isinstance(result, float):
            pass
        else:
            raise Exception('Not supported result type: %s'%str(result))

    def floatupdate(self, result, elem, retval):
        if isinstance(result, bool):
            if result:
                pass
            else:
                pass
        elif isinstance(result, int):
            pass
        elif isinstance(result, float):
            pass
        else:
            raise Exception('Not supported result type: %s'%str(result))

    def dbexport(self):
        if isinstance(self.minval, int):
            return [ ( 'bithitmiss', str(self.choicedb['bithitmiss']) ), \
                ( 'lenbits', str(self.choicedb['lenbits']) ) ]
        else:
            return []
