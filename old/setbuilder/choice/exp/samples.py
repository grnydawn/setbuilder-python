import sys
import numpy as np

SB_MAXINT_VALUE = sys.maxint if 'maxint' in dir(sys) else sys.maxsize
SB_MININT_VALUE = -sys.maxint-1 if 'maxint' in dir(sys) else -sys.maxsize-1

NBATCH = 10
BITLEN = 32

boolexpr1 = lambda x: x > 1
boolexpr2 = lambda x: x < 10
binstr = lambda n,l: format(2**l + n, 'b')[-l:]
binary = lambda n,l: map(int, list(binstr(n,l)))

def nonlin(x,deriv=False):
    if(deriv==True):
        return x*(1-x)

    return 1/(1+np.exp(-x))

X = np.array([binary(10, BITLEN), binary(-10, BITLEN)])

y = np.array([[1],
            [0]])

np.random.seed(1)

# randomly initialize our weights with mean 0
syn0 = 2*np.random.random((BITLEN,BITLEN)) - 1
syn1 = 2*np.random.random((BITLEN,1)) - 1


def forward(data):
    l1 = nonlin(np.dot(data,syn0))
    l2 = nonlin(np.dot(l1,syn1))
    return l2, l1

def test(expected, l2):
    error = expected - l2
    return error

def backward(error, l2, l1, l0, w1, w0):

    # in what direction is the target value?
    # were we really sure? if so, don't change too much.
    l2_delta = error*nonlin(l2, deriv=True)

    # how much did each l1 value contribute to the l2 error (according to the weights)?
    l1_error = l2_delta.dot(syn1.T)

    # in what direction is the target l1?
    # were we really sure? if so, don't change too much.
    l1_delta = l1_error * nonlin(l1, deriv=True)

    w1 += l1.T.dot(l2_delta)
    w0 += l0.T.dot(l1_delta)

for j in xrange(60000):

    samples = []
    while len(samples) < NBATCH
        val = random.randint(random.randint)
    # repeat forward until we have enough samples
    # samples are chosen based on l2 values
    l2, l1 = forward(X)


    # test using boolean expression(s) and produce errors
    # the error feedback to backward routine

    err = test(y, l2)

    if (j% 10000) == 0:
        print l2
        print "Error:" + str(np.mean(np.abs(err)))

    backward(err, l2, l1, X, syn1, syn0)




