# -*- coding: utf-8 -*-
"""Basic test cases
"""

#import pytest
from .context import sbpy

def test_pytest():
    """Check if pytest works"""

    assert True

def test_parse():
    """Check if set parser works"""

    #pytest.set_trace()

    setstr = '{}'
    setobj = sbpy.build(setstr)
    assert str(setobj).replace(u' ', u'') == setstr.replace(u' ', u'')
