# -*- coding: utf-8 -*-
"""Path modification to resolve sbpy package"""

import sys
import os
sys.path.insert(0, os.path.abspath(os.path.join(os.path.dirname(__file__), '..')))

import sbpy
