"""Testgen dev module
"""

import inspect
import functools
import pytest
import sbpy

class TG_DOMAIN(sbpy.Set):
    pass

class TG_Exception(Exception):
    pass

def gentestcases(setnote, max_cases, case_reuse, frame):

    #pytest.set_trace()
    setobj = sbpy.build(setnote, setglobals=frame.f_globals, setlocals=frame.f_locals)
    for case in setobj.choice(max_cases, case_reuse):
        yield case

def testargs(setnote, varargs, kwargs, frame):

    setobj = sbpy.build(setnote, setglobals=frame.f_globals, setlocals=frame.f_locals)
    #pytest.set_trace()

    if varargs not in setobj:
        raise Exception('Test failure.')

def testretval(setnote, func, varargs, kwargs, ret, frame):

    # check if ret in setnote
    argspec = inspect.getargspec(func)
    newlocals = dict(frame.f_locals)
    newlocals.update(dict(zip(argspec.args, varargs)))

    setobj = sbpy.build(setnote, setglobals=frame.f_globals, setlocals=newlocals)

    if ret not in setobj:
        raise Exception('Test failure.')


def function(domain, codomain, check_args=None, check_retval=None, check_mapping=None, max_cases=10, case_reuse=False):
    """test decorator for function
    """

    #sbpy.msg(domain)
    #sbpy.msg(codomain)
    mappingframe = inspect.currentframe().f_back

    def decorator(decoratedfunc):

        @functools.wraps(decoratedfunc)
        def wrapper(*wrappervargs, **wrapperkwargs):

            df_ret = decoratedfunc(*wrappervargs, **wrapperkwargs)

            #argspec = inspect.getargspec(decoratedfunc)
            #if argspec.varargs or argspec.keywords:
            #    pytest.set_trace()

            # monitoring input arguments
            if check_args:
                # check output
                testargs(domain, wrappervargs, wrapperkwargs, mappingframe)

            # checking output value
            if check_mapping:
                # iterate through generated test cases
                for casevargs, casekwargs in gentestcases(domain, max_cases, case_reuse, mappingframe):

                    # new local namespace
                    newlocals = dict(mappingframe.f_locals)
                    newlocals.update( { 'decoratedfunc': decoratedfunc,
                        'casevargs': casevargs, 'casekwargs': casekwargs } )

                    # execute function on its own namespace with adjustment
                    ret = eval('decoratedfunc(*casevargs, **casekwargs)',
                        mappingframe.f_globals(), newlocals)

                    # check output
                    testretval(codomain, decoratedfunc, casevargs, casekwargs, ret, mappingframe)

            if check_retval:
                # check output
                testretval(codomain, decoratedfunc, wrappervargs, wrapperkwargs, df_ret, mappingframe)

            return df_ret
        return wrapper
    return decorator
