"""A basic testgen test module

In this module, basic scenarios of testgen will be invoked.

Forward and backward mapping in sbpy set specifications.

{ sevar | set test }
{ sevar | set test where wheresetvar in Set }
{ sevar | set test | forward mapping where wheresetvar in Set }
{ sevar | set test | forward mapping | backward mapping where wheresetvar in Set }

{ x | x.value > 0 | x.value = y | y = x.value where y in SB_REALNUM_SET }

{ sevar | set test }
{ sevar | set test where forward mapping | backward mapping | wheresetvar in Set }

{ x | x.value > 0 where x = Cls(y) | y = x.value | y in SB_REALNUM_SET }
If we have above spec, choice could be backward and forward
- find candidates of elements that satisfy x.value >0
- Using the elems, calculate y
- now ask y in SB_REALNUM_SET
- once the asking reaches to PrimitiveSet, we can use forward mapping for verification too
"""

import pytest
from .testgen import function, TG_DOMAIN, TG_Exception

def test_inout():
    """basic input output test case.

    """

    @function( '{ x | x > 0 }', '{ y | y == number + 1 }', check_args=True, check_retval=True )
    def addone(number):
        return number + 1

    #assert addone(1) == 2
    addone(1)

def test_class():
    """basic input output test case.

    """

    class NumCls(object):
        def __init__(self, value):
            self.value = value

    @function( ' \
        { (x1, x2) | \
            x1.value > 0 and x1.value > 0 | \
            x1 = NumCls(y1); x2 = NumCls(y2) \
            where y1 in SB_REALNUM_SET and y2 in SB_REALNUM_SET }', \
        '{ y | y == cls1.value + cls2.value }', \
        check_args=True, check_retval=True, check_mapping=True )
    def sumcls(cls1, cls2):
        return cls1.value + cls2.value

    a = NumCls(1)
    b = NumCls(1)

    sumcls(a, b)
