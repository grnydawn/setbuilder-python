default: test

init:
	pip install -r requirements.txt --user

test:
	py.test -v --capture=no tests

style:
	pylint --rcfile=pylint.rc sbpy tests

html:
	make -C docs html

.PHONY: init test style html
